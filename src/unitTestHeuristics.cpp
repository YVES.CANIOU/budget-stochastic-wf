// Include necessary headers

#include "unitTestHeuristics.h"


//this ignor file compares all the ellements of the file exept empty lines, which are ignored by default
std::vector<std::string> ignoreStringsNothing = {};

//with this ignor list, aonly the order of the tasks and on which machine they where executed is compared 
std::vector<std::string> ignoreStringsJustOrdoAndMachine = {
    "ids :",
    "generation",
    "number"
};

//with this ignor list, aonly the order of the tasks is compared 
std::vector<std::string> ignoreStringsJustOrdo = {
    "ids :",
    "generation",
    "number",
    "bare"
};


template<typename InputIterator1, typename InputIterator2>
bool
range_equal(InputIterator1 first1, InputIterator1 last1,
        InputIterator2 first2, InputIterator2 last2)
{
    while(first1 != last1 && first2 != last2)
    {
        if(*first1 != *first2) return false;
        ++first1;
        ++first2;
    }
    return (first1 == last1) && (first2 == last2);
}


/**
 * @brief Compares two files line by line, ignoring empty lines and lines containing specified strings.
 *
 * @param filename1         The path to the first file.
 * @param filename2         The path to the second file.
 * @param ignoreList A vector of strings to ignore when comparing lines.
 *
 * @return `true` if the files are identical after ignoring empty lines and lines containing ignore strings,
 *         `false` otherwise.
 */
bool compareFilesSimple(const std::string& filename1, const std::string& filename2)
{
    std::ifstream file1(filename1);
    std::ifstream file2(filename2);

    std::istreambuf_iterator<char> begin1(file1);
    std::istreambuf_iterator<char> begin2(file2);

    std::istreambuf_iterator<char> end;

    return range_equal(begin1, end, begin2, end);
}


/**
 * @brief Compares two files line by line, ignoring empty lines and lines containing specified strings.
 *
 * @param filename1         The path to the first file.
 * @param filename2         The path to the second file.
 * @param ignoreList A vector of strings to ignore when comparing lines.
 *
 * @return `true` if the files are identical after ignoring empty lines and lines containing ignore strings,
 *         `false` otherwise.
 */
bool compareFilesIgnoreListe(const std::string& filename1, const std::string& filename2, const std::vector<std::string>& ignoreList)
{
    std::ifstream file1(filename1);
    std::ifstream file2(filename2);

    std::string line1, line2;

    while (std::getline(file1, line1) && std::getline(file2, line2))
    {
        // skip empty lines
        while (line1.empty() && std::getline(file1, line1)) {}
        while (line2.empty() && std::getline(file2, line2)) {}

        // Ignore lines containing specified strings
        if (std::find_if(ignoreList.begin(), ignoreList.end(), [&line1](const std::string& ignore) {
                return line1.find(ignore) != std::string::npos;
            }) != ignoreList.end())
            continue;

        if (line1 != line2)
            return false;
    }

    // If one file has more lines than the other, they are not equal
    return !std::getline(file1, line1) && !std::getline(file2, line2);
}


TEST_F(toolsTest, compareFilesSimpleSameFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFiles/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFiles/file2.txt";



    bool compare = compareFilesSimple(file1, file2);



    ASSERT_TRUE(compare);
}

TEST_F(toolsTest, compareFilesSameFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFiles/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFiles/file2.txt";



    bool compare = compareFilesIgnoreListe(file1, file2, ignoreStringsNothing);



    ASSERT_TRUE(compare);
}

TEST_F(toolsTest, compareFilesSimpleDifferntFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFiles/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFiles/file2.txt";



    bool compare = compareFilesSimple(file1, file2);



    ASSERT_FALSE(compare);
}

TEST_F(toolsTest, compareFilesDifferntFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFiles/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFiles/file2.txt";



    bool compare = compareFilesIgnoreListe(file1, file2, ignoreStringsNothing);



    ASSERT_FALSE(compare);
}


TEST_F(toolsTest, compareFilesSimpleSameFilesIfYouIgnorLinesFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFilesIfYouIgnorLines/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFilesIfYouIgnorLines/file2.txt";



    bool compare = compareFilesSimple(file1, file2);



    ASSERT_FALSE(compare);
}

TEST_F(toolsTest, compareFilesSameFilesIfYouIgnorLinesFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFilesIfYouIgnorLines/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/sameFilesIfYouIgnorLines/file2.txt";



    bool compare = compareFilesIgnoreListe(file1, file2, ignoreStringsJustOrdoAndMachine);



    ASSERT_TRUE(compare);
}

TEST_F(toolsTest, compareFilesSimpledifferentFilesIgnorFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFilesIgnor/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFilesIgnor/file2.txt";



    bool compare = compareFilesSimple(file1, file2);



    ASSERT_FALSE(compare);
}

TEST_F(toolsTest, compareFilesdifferentFilesIgnorFiles) {

    std::string file1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFilesIgnor/file1.txt";
    std::string file2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testCompareFiles/differentFilesIgnor/file2.txt";



    bool compare = compareFilesIgnoreListe(file1, file2, ignoreStringsJustOrdoAndMachine);



    ASSERT_FALSE(compare);
}

/**********************************************************************************************/



TEST_F(HeuristicsTest, MinMinSchedulesTasksInOrderSimple) {
    // Set up a fake argc and argv
    std::vector<char*> args = {
        (char*)"./wrench-example-condor-grid-universe",
        (char*)"/home/nanachibestwaifu/WORKDIR/wrench/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testSimple/configTestSimple.json"
    };

    // Path to the result generated by the simulation
    std::string savePathOrdo1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testSimple/results/outputOrdo1.txt";
    std::string savePathOrdo2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testSimple/results/outputOrdo2.txt";

    // Path to the predefined expected results of the simulation 
    std::string expectedOrdoPath1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testSimple/results/expectedOrdo1.txt";
    std::string expectedOrdoPath2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testSimple/results/expectedOrdo2.txt";

    // Create argc and argv variables
    int argc = args.size();
    std::vector<char*> argv;
    for (auto& arg : args) {
        argv.push_back(arg);
    }

    // Fork the process
    pid_t pid = fork();

    if (pid == 0) {
        // Child process
        LaunchDoubleSimulation(argc, argv.data(), savePathOrdo1, savePathOrdo2);
    } else if (pid > 0) {
        // Parent process
        int status;
        waitpid(pid, &status, 0);
        bool compare1 = compareFilesIgnoreListe(savePathOrdo1, expectedOrdoPath1, ignoreStringsJustOrdoAndMachine);
        bool compare2 = compareFilesIgnoreListe(savePathOrdo2, expectedOrdoPath2, ignoreStringsJustOrdoAndMachine);

        // Assert that the tasks are scheduled in the expected order
        ASSERT_TRUE(compare1);
        ASSERT_TRUE(compare2);
    }
}

//TODO voir si il doit etre vrai celui ci
TEST_F(HeuristicsTest, MinMinSchedulesTasksUnorderedSimple) {
    // Set up a fake argc and argv
    std::vector<char*> args = {
        (char*)"./wrench-example-condor-grid-universe",
        (char*)"/home/nanachibestwaifu/WORKDIR/wrench/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testUnorderedTask/configTestSimple.json"
    };

    // Path to the result generated by the simulation
    std::string savePathOrdo1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testUnorderedTask/results/outputOrdo1.txt";
    std::string savePathOrdo2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testUnorderedTask/results/outputOrdo2.txt";

    // Path to the predefined expected results of the simulation 
    std::string expectedOrdoPath1 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testUnorderedTask/results/expectedOrdo1.txt";
    std::string expectedOrdoPath2 = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testFiles/testOrdonancement/testUnorderedTask/results/expectedOrdo2.txt";

    // Create argc and argv variables
    int argc = args.size();
    std::vector<char*> argv;
    for (auto& arg : args) {
        argv.push_back(arg);
    }
        // LaunchDoubleSimulation(argc, argv.data(), savePathOrdo1, savePathOrdo2);

    // Fork the process
    pid_t pid = fork();

    if (pid == 0) {
        // Child process
        LaunchDoubleSimulation(argc, argv.data(), savePathOrdo1, savePathOrdo2);
    } else if (pid > 0) {
        // Parent process
        int status;
        waitpid(pid, &status, 0);
        bool compare1 = compareFilesIgnoreListe(savePathOrdo1, expectedOrdoPath1, ignoreStringsJustOrdo);
        bool compare2 = compareFilesIgnoreListe(savePathOrdo2, expectedOrdoPath2, ignoreStringsJustOrdo);

        // Assert that the tasks are scheduled in the expected order
        ASSERT_TRUE(compare1);
        ASSERT_TRUE(compare2);
    }
}



int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
