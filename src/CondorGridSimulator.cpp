/**
 * Copyright (c) 2020. The WRENCH Team.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

#include "CondorGridSimulator.h"
#include "infoSim.h"

std::vector<std::tuple<std::string, double>> listHS;

static bool ends_with(const std::string &str, const std::string &suffix) {
    return str.size() >= suffix.size() && 0 == str.compare(str.size() - suffix.size(), suffix.size(), suffix);
}


// Function to parse the WML file and extract host information
std::vector<std::tuple<std::string, double>> getHostsFromXML(const std::string& file_path) {
    std::vector<std::tuple<std::string, double>> host_list;

    std::ifstream file(file_path);
    if (!file.is_open()) {
        std::cout << "Error opening file: " << file_path << std::endl;
        return host_list;
    }

    std::string line;
    while (std::getline(file, line)) {
        std::size_t host_start = line.find("<host id=");
        if (host_start != std::string::npos) {
            std::size_t id_start = line.find("\"", host_start);
            std::size_t id_end = line.find("\"", id_start + 1);
            std::string host_id = line.substr(id_start + 1, id_end - id_start - 1);

            std::size_t speed_start = line.find("speed=\"", id_end);
            std::size_t speed_end = line.find("\"", speed_start + 7);
            std::string speed = line.substr(speed_start + 7, speed_end - speed_start - 7);

            // Normalize the speed value to flops
            double normalized_speed = 0.0;
            std::string speed_unit = speed.substr(speed.length() - 2);
            double speed_value = std::stod(speed.substr(0, speed.length() - 2));

            if (speed_unit == "kf") {
                normalized_speed = speed_value * 1e3;
            } else if (speed_unit == "Mf") {
                normalized_speed = speed_value * 1e6;
            } else if (speed_unit == "Gf") {
                normalized_speed = speed_value * 1e9;
            } else if (speed_unit == "Tf") {
                normalized_speed = speed_value * 1e12;
            }
            
            host_list.push_back(std::make_tuple(host_id, normalized_speed));
        }
    }

    file.close();
    return host_list;
}


/**
 * @brief      modifies the argv and argc to match the original format of the commande to launch a simulation.
 *  This is done because the initialisation uses directly argc and argv
 *
 * @param      argc          The count of arguments
 * @param      argv          The arguments array
 * @param[in]  platformPath  The platform path
 * @param[in]  workflowPath  The workflow path
 */
void modifyArgv(int& argc, char* argv[], const std::string& platformPath, const std::string& workflowPath) {
    // Create a vector to hold the modified arguments
    std::vector<std::string> modifiedArgs;

    // Add the original argument to the vector
    if (argc > 0)
    {
        modifiedArgs.push_back(argv[0]);
        
    }

    // Add the additional strings to the vector
    modifiedArgs.push_back(workflowPath);
    modifiedArgs.push_back(platformPath);

    // Update argc with the new count
    argc = modifiedArgs.size();

    // Update argv with the modified arguments
    for (int i = 0; i < argc; i++) {
        argv[i] = new char[modifiedArgs[i].size() + 1];
        strcpy(argv[i], modifiedArgs[i].c_str());
    }
}




SavedOrdonancement launchSecondSimulation(int argc, char **argv, SavedOrdonancement savedOrdonan, ReadConfig conf) {

        SavedOrdonancement sav;
        auto simulationRepeat = wrench::Simulation::createSimulation();
        simulationRepeat->init(&argc, argv);

        /* Parsing of the command-line arguments for this WRENCH simulation */
        if (argc != 3) {
            std::cerr << "Usage: " << argv[0] << " <xml platform file> <workflow file> [--log=custom_wms.threshold=info]" << std::endl;
            exit(1);
        }
        /* The first argument is the platform description file, written in XML following the SimGrid-defined DTD */
        char *platform_file = argv[1];
        /* The second argument is the workflow description file, written in JSON using WfCommons's WfFormat format */
        char *workflow_file = argv[2];




        /* Reading and parsing the workflow description file to create a wrench::Workflow object */
        std::cerr << "Loading workflow..." << std::endl;
        std::shared_ptr<wrench::Workflow> workflow;
        // std::shared_ptr<wrench::Workflow> workflow_repeat;
        if (ends_with(workflow_file, "json")) {
            workflow = wrench::WfCommonsWorkflowParser::createWorkflowFromJSON(workflow_file, "100Gf");
            // workflow_repeat = wrench::WfCommonsWorkflowParser::createWorkflowFromJSON(workflow_file, "100Gf");
        } else {
            std::cerr << "Workflow file name must end with '.json'" << std::endl;
            exit(1);
        }
        std::cerr << "The workflow has " << workflow->getNumberOfTasks() << " tasks " << std::endl;
        std::cerr.flush();


        /* Reading and parsing the platform description file to instantiate a simulated platform */
        std::cerr << "Instantiating SimGrid platform..." << std::endl;
        simulationRepeat->instantiatePlatform(platform_file);
        // simulationRepeat->instantiatePlatform(platform_file);




    /* Instantiate a storage service, to be started on some host in the simulated platform,
     * and adding it to the simulation.  A wrench::StorageService is an abstraction of a service on
     * which files can be written and read.  This particular storage service, which is an instance
     * of wrench::SimpleStorageService, is started on WMSHost in the
     * platform (platform/batch_platform.xml), which has an attached disk called large_disk. The SimpleStorageService
     * is a barebone storage service implementation provided by WRENCH.
     * Throughout the simulation execution, input/output files of workflow tasks will be located
     * in this storage service.
     */
        std::cerr << "Instantiating a SimpleStorageService on WMSHost..." << std::endl;
    auto storage_service = simulationRepeat->add(new wrench::SimpleStorageService(
            "WMSHost", {"/"}, {{wrench::SimpleStorageServiceProperty::BUFFER_SIZE, "50MB"}}, {}));


    /* Create a list of compute services that will be used by the WMS */
    std::set<std::shared_ptr<wrench::ComputeService>> compute_services;


    /* Get a vector of all the hosts in the simulated platform */
    std::vector<std::string> hostname_list = wrench::Simulation::getHostnameList();

    /* Instantiate and add to the simulation a batch_standard_and_pilot_jobs service, to be started on some host in the simulation platform.
     * A batch_standard_and_pilot_jobs service is an abstraction of a compute service that corresponds to
     * batch_standard_and_pilot_jobs-scheduled platforms in which jobs are submitted to a queue and dispatched
     * to compute nodes according to various scheduling algorithms.
     * In this example, this particular batch_standard_and_pilot_jobs service has no scratch storage space (mount point = "").
     * The next argument to the constructor
     * shows how to configure particular simulated behaviors of the compute service via a property
     * list. In this case, we use the conservative_bf_core_level scheduling algorithm which implements
     * conservative backfilling at the core level (i.e., two jobs can shared a compute node by using different cores on it).
     * The last argument to the constructor makes it possible to specify various control message sizes.
     * In this example, one specifies that the message that will be send to the service to
     * terminate it will be 2048 bytes. See the documentation to find out all available
     * configurable properties for each kind of service.
     */
    std::shared_ptr<wrench::BatchComputeService> batch_compute_service;
#ifndef ENABLE_BATSCHED
    std::string scheduling_algorithm = "conservative_bf_core_level";
#else
    std::string scheduling_algorithm = "conservative_bf";
#endif
    try {
        batch_compute_service = simulationRepeat->add(new wrench::BatchComputeService(
                {"BatchHeadNode"}, {{"BatchNode1"}, {"BatchNode2"}}, "",
                {{wrench::BatchComputeServiceProperty::BATCH_SCHEDULING_ALGORITHM, scheduling_algorithm}},
                {{wrench::BatchComputeServiceMessagePayload::STOP_DAEMON_MESSAGE_PAYLOAD, 2048}}));
    } catch (std::invalid_argument &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        std::exit(1);
    }
/*IL FAUT SANS DOUTE VOIR AVEC CES PARAMETRES POUR MODIFIER LE NOMBRE DE TACHES RUNABLES
 * 
 * VOIR AUSSI SI J'AI BESOIN DU BATCH COMPUTING, JE SUIS PAS SUR MAIS C PEUT ETRE CA QUI FOUT LE BORDEL
 */
        // std::shared_ptr<wrench::CloudComputeService> cloud_compute_service;
        // // std::shared_ptr<wrench::CloudComputeService> cloud_compute_service_repeat;
        // try {
        //     cloud_compute_service = simulationRepeat->add(new wrench::CloudComputeService(
        //             {"CloudHeadNode"}, {{"CloudNode1"}, {"CloudNode2"}, {"CloudNode3"}, {"CloudNode4"}}, "", {},
        //             {{wrench::CloudComputeServiceMessagePayload::STOP_DAEMON_MESSAGE_PAYLOAD, 1024}}));
        // } catch (std::invalid_argument &e) {
        //     std::cerr << "Error: " << e.what() << std::endl;
        //     std::exit(1);
        // }


    /* Instantiate a virtualized cluster compute service, and add it to the simulation.
     * A wrench::VirtualizedClusterComputeService is an abstraction of a compute service that corresponds
     * to a virtualized_cluster that responds to VM creating requests, and each VM exposes a "bare-metal" compute service.
     * This particular service is started on VirtualizedClusterProviderHost, uses VirtualizedClusterHost1 and VirtualizedClusterHost2
     * as hardware resources, and has no scratch storage space (mount point argument = "").
     * This means that tasks running on this service will access data only from remote storage services. */


    //***********************************
    std::shared_ptr<infoSim> indoSimPointer = std::shared_ptr<infoSim>(new infoSim(simulationRepeat, workflow, storage_service, &conf));
    // std::shared_ptr<infoSim> indoSimPointer = infoSim::create(simulationRepeat, workflow, simpleStorageService);//set up the infos about 
    // indoSimPointer->printHostNameAndSpeed();
    // indoSimPointer->printUsableHostNameAndType();
    //***********************************


    std::cerr << "Instantiating a VirtualizedClusterComputeService on VirtualizedClusterProviderHost..." << std::endl;
        //TODO ajouter la listes des hosts dans infoSim
    std::vector<std::string> virtualized_cluster_hosts = indoSimPointer->getUsableHostes();//{"VirtualizedClusterHost1", "VirtualizedClusterHost2"};
    auto virtualized_cluster_service = simulationRepeat->add(new wrench::VirtualizedClusterComputeService(
            "VirtualizedClusterProviderHost", virtualized_cluster_hosts, "/SCRATCH/", {}, {}));


        std::cerr << "Instantiating a WMS on WMSHost..." << std::endl;
        // wrench::SecondeSimulation TestSimulaiton;
        auto secondSim = new wrench::SecondeSimulation(workflow,
                                                       batch_compute_service,
                                                       storage_service,
                                                       virtualized_cluster_service,
                                                       {"WMSHost"});
        secondSim->addOrdo(savedOrdonan);
        secondSim->addConfig(conf);
        // secondSim->listHostSpeed = listHS;
        auto wms = simulationRepeat->add(secondSim);

        //  hosts among other things 

        std::string file_registry_service_host = hostname_list[(hostname_list.size() > 2) ? 1 : 0];
        std::cerr << "Instantiating a FileRegistryService on " << file_registry_service_host << "..." << std::endl;
        auto file_registry_service =
                simulationRepeat->add(new wrench::FileRegistryService(file_registry_service_host));


    /* It is necessary to store, or "stage", input files that only input. The getInputFiles()
     * method of the Workflow class returns the set of all workflow files that are not generated
     * by workflow tasks, and thus are only input files. These files are then staged on the storage service. */
    std::cerr << "Staging task input files..." << std::endl;
    for (auto const &f: workflow->getInputFiles()) {
        simulationRepeat->stageFile(f, storage_service);
    }




        //***********************************

        wms->myInfoSim = indoSimPointer;
        //***********************************

        std::cerr << "Launching the Simulation..." << std::endl;
        try {
            simulationRepeat->launch();
        } catch (std::runtime_error &e) {
            std::cerr << "Exception: " << e.what() << std::endl;
            exit(1) ;
        }
        std::cerr << "Simulation done!" << std::endl;

        // void dumpUnifiedJSON(const std::shared_ptr<Workflow> &workflow, std::string file_path, bool include_platform = false, bool include_workflow_exec = true, bool include_workflow_graph = false, bool include_energy = false, bool generate_host_utilization_layout = false, bool include_disk = false, bool include_bandwidth = false)
        /* Dump it all to a JSON file */
        simulationRepeat->getOutput().dumpUnifiedJSON(workflow, "/tmp/wrench.json",
                            /*include_platform*/false,
                            /*include_workflow_exec*/true,
                            /*include_workflow_graph*/true,
                            /*include_energy*/false ,
                            /*generate_host_utilization_layout*/true,
                            /*include_disk*/true,
                            /*include_bandwidth*/true);

        sav = SavedOrdonancement(wms->sheduledTaskList, wms->sheduledHostList, wms->generations, wms->nbFreeCorePerGeneration);
        // sav.print();

        auto trace = simulationRepeat->getOutput().getTrace<wrench::SimulationTimestampTaskCompletion>();
        for (auto const &item: trace) {
            std::cerr << "Task " << item->getContent()->getTask()->getID() << " completed at time " << item->getDate() << std::endl;
        }
    // }
    return sav;
}



//mentionner en https://forge.univ-lyon1.fr/YVES.CANIOU/budget-stochastic-wf-static-sched/-/blob/master/tools/analyse/AnalysisPaper.R
//Wf,type,algo, nbTsk, tmpWf,costWf, budget_residuel, budget_ini, nb_VM_used, prct_VM_used, worstTCost, worstTTime, diffR, pcVarTeor, pcVarReal, seed, worstCCost, worsCTime
















SavedOrdonancement lanchFirstSimulation(int argc, char **argv, ReadConfig conf)
{


    // Create and initialize a simulation
    auto simulation = wrench::Simulation::createSimulation();
    // auto simulationRepeat = wrench::Simulation::createSimulation();

    /*
     * Initialization of the simulation, which may entail extracting WRENCH-specific and
     * Simgrid-specific command-line arguments that can modify general simulation behavior.
     * Two special command-line arguments are --help-wrench and --help-simgrid, which print
     * details about available command-line arguments.
     */
    simulation->init(&argc, argv);
    // simulationRepeat->init(&argc, argv);





    /* Parsing of the command-line arguments for this WRENCH simulation */
    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " <xml platform file> <workflow file> [--log=custom_wms.threshold=info]" << std::endl;
        exit(1);
    }
    /* The first argument is the platform description file, written in XML following the SimGrid-defined DTD */
    char *platform_file = argv[1];
    /* The second argument is the workflow description file, written in JSON using WfCommons's WfFormat format */
    char *workflow_file = argv[2];

    /* Reading and parsing the workflow description file to create a wrench::Workflow object */
    std::cerr << "Loading workflow..." << std::endl;
    std::shared_ptr<wrench::Workflow> workflow;
    // std::shared_ptr<wrench::Workflow> workflow_repeat;
    if (ends_with(workflow_file, "json")) {
        workflow = wrench::WfCommonsWorkflowParser::createWorkflowFromJSON(workflow_file, "100Gf");
        // workflow_repeat = wrench::WfCommonsWorkflowParser::createWorkflowFromJSON(workflow_file, "100Gf");
    } else {
        std::cerr << "Workflow file name must end with '.json'" << std::endl;
        exit(1);
    }
    std::cerr << "The workflow has " << workflow->getNumberOfTasks() << " tasks " << std::endl;
    std::cerr.flush();


    /* Reading and parsing the platform description file to instantiate a simulated platform */
    std::cerr << "Instantiating SimGrid platform..." << std::endl;
    simulation->instantiatePlatform(platform_file);
    // simulationRepeat->instantiatePlatform(platform_file);




    /* Instantiate a storage service, to be started on some host in the simulated platform,
     * and adding it to the simulation.  A wrench::StorageService is an abstraction of a service on
     * which files can be written and read.  This particular storage service, which is an instance
     * of wrench::SimpleStorageService, is started on WMSHost in the
     * platform (platform/batch_platform.xml), which has an attached disk called large_disk. The SimpleStorageService
     * is a barebone storage service implementation provided by WRENCH.
     * Throughout the simulation execution, input/output files of workflow tasks will be located
     * in this storage service.
     */
        std::cerr << "Instantiating a SimpleStorageService on WMSHost..." << std::endl;
    auto storage_service = simulation->add(new wrench::SimpleStorageService(
            "WMSHost", {"/"}, {{wrench::SimpleStorageServiceProperty::BUFFER_SIZE, "50MB"}}, {}));
    // std::cerr << "Instantiating a SimpleStorageService on WMSHost " << std::endl;
    // auto storage_service = simulation->add(new wrench::SimpleStorageService({"WMSHost"},/*AVOIRPLUSTARD{""}*/ {"/"}, {{wrench::SimpleStorageServiceProperty::BUFFER_SIZE, "50MB"}}, {}));
    // // auto storage_service_repeat = simulationRepeat->add(new wrench::SimpleStorageService({"WMSHost"}, {"/"}));
    // storage_services.insert(storage_service);
    // // storage_services_repeat.insert(storage_service_repeat);


    /* Create a list of compute services that will be used by the WMS */
    std::set<std::shared_ptr<wrench::ComputeService>> compute_services;


    /* Get a vector of all the hosts in the simulated platform */
    std::vector<std::string> hostname_list = wrench::Simulation::getHostnameList();

    /* Instantiate and add to the simulation a batch_standard_and_pilot_jobs service, to be started on some host in the simulation platform.
     * A batch_standard_and_pilot_jobs service is an abstraction of a compute service that corresponds to
     * batch_standard_and_pilot_jobs-scheduled platforms in which jobs are submitted to a queue and dispatched
     * to compute nodes according to various scheduling algorithms.
     * In this example, this particular batch_standard_and_pilot_jobs service has no scratch storage space (mount point = "").
     * The next argument to the constructor
     * shows how to configure particular simulated behaviors of the compute service via a property
     * list. In this case, we use the conservative_bf_core_level scheduling algorithm which implements
     * conservative backfilling at the core level (i.e., two jobs can shared a compute node by using different cores on it).
     * The last argument to the constructor makes it possible to specify various control message sizes.
     * In this example, one specifies that the message that will be send to the service to
     * terminate it will be 2048 bytes. See the documentation to find out all available
     * configurable properties for each kind of service.
     */
    std::shared_ptr<wrench::BatchComputeService> batch_compute_service;
#ifndef ENABLE_BATSCHED
    std::string scheduling_algorithm = "conservative_bf_core_level";
#else
    std::string scheduling_algorithm = "conservative_bf";
#endif
    try {
        batch_compute_service = simulation->add(new wrench::BatchComputeService(
                {"BatchHeadNode"}, {{"BatchNode1"}, {"BatchNode2"}}, "",
                {{wrench::BatchComputeServiceProperty::BATCH_SCHEDULING_ALGORITHM, scheduling_algorithm}},
                {{wrench::BatchComputeServiceMessagePayload::STOP_DAEMON_MESSAGE_PAYLOAD, 2048}}));
    } catch (std::invalid_argument &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        std::exit(1);
    }


    //***********************************

    std::shared_ptr<infoSim> indoSimPointer = std::shared_ptr<infoSim>(new infoSim(simulation, workflow, storage_service, &conf));
    // std::shared_ptr<infoSim> indoSimPointer = infoSim::create(simulation);//set up the infos about 
    // indoSimPointer->printHostNameAndSpeed();
    // indoSimPointer->printUsableHostNameAndType();
    //***********************************

    /* Instantiate a virtualized cluster compute service, and add it to the simulation.
     * A wrench::VirtualizedClusterComputeService is an abstraction of a compute service that corresponds
     * to a virtualized_cluster that responds to VM creating requests, and each VM exposes a "bare-metal" compute service.
     * This particular service is started on VirtualizedClusterProviderHost, uses VirtualizedClusterHost1 and VirtualizedClusterHost2
     * as hardware resources, and has no scratch storage space (mount point argument = "").
     * This means that tasks running on this service will access data only from remote storage services. */
        std::cerr << "Instantiating a VirtualizedClusterComputeService on VirtualizedClusterProviderHost..." << std::endl;
    std::vector<std::string> virtualized_cluster_hosts = indoSimPointer->getUsableHostes();//{"VirtualizedClusterHost1", "VirtualizedClusterHost2"};
    auto virtualized_cluster_service = simulation->add(new wrench::VirtualizedClusterComputeService(
            "VirtualizedClusterProviderHost", virtualized_cluster_hosts, "/SCRATCH/", {}, {}));


    // std::cerr << "Instantiating a VirtualizedClusterComputeService on VirtualizedClusterProviderHost..." << std::endl;
    // std::vector<std::string> virtualized_cluster_hosts = {"VirtualizedClusterHost1", "VirtualizedClusterHost2"};
    // auto virtualized_cluster_service = simulation->add(new wrench::VirtualizedClusterComputeService(
    //         "VirtualizedClusterProviderHost", virtualized_cluster_hosts, "/SCRATCH/", {}, {}));

    /* Instantiate a WMS (which is an ExecutionController really), to be started on some host (wms_host), which is responsible
     * for executing the workflow.
     *
     * The WMS implementation is in SimpleWMS.[cpp|h].
     */
    // std::cerr << "Instantiating a WMS on WMSHost..." << std::endl;
    // auto wms = simulation->add(
    //         new wrench::CondorWMS(workflow, batch_compute_service,
    //                               cloud_compute_service, storage_service, {"WMSHost"}));

    std::cerr << "Instantiating a WMS on WMSHost..." << std::endl;
    auto condo = new wrench::CondorWMS(workflow,
                                       batch_compute_service,
                                       virtualized_cluster_service,
                                       storage_service,
                                       "WMSHost");
                                       // "VirtualizedClusterProviderHost"); //AVOIRPLUSTARD
    condo->addConfig(conf);
    condo->listHostSpeed = listHS;
    // for (string temp : wrench::Simulation::getHostnameList())
    //     cerr << temp << endl;
    auto wms = simulation->add(condo);



    /* Instantiate a file registry service to be started on WMSHost. This service is
     * essentially a replica catalog that stores <file , storage service> pairs so that
     * any service, in particular a WMS, can discover where workflow files are stored. */
    // std::cerr << "Instantiating a FileRegistryService on WMSHost ..." << std::endl;
    // auto file_registry_service = new wrench::FileRegistryService("WMSHost");
    // simulation->add(file_registry_service);

    /* Instantiate a file registry service to be started on some host. This service is
     * essentially a replica catalog that stores <file , storage service> pairs so that
     * any service, in particular a WMS, can discover where workflow files are stored.
     */
    std::string file_registry_service_host = hostname_list[(hostname_list.size() > 2) ? 1 : 0];
    std::cerr << "Instantiating a FileRegistryService on " << file_registry_service_host << "..." << std::endl;
    auto file_registry_service =
            simulation->add(new wrench::FileRegistryService(file_registry_service_host));



    /* It is necessary to store, or "stage", input files that only input. The getInputFiles()
     * method of the Workflow class returns the set of all workflow files that are not generated
     * by workflow tasks, and thus are only input files. These files are then staged on the storage service. */
    // std::cerr << "Staging task input files..." << std::endl;
    // for (auto const &f: workflow->getInputFiles()) {
    //     simulation->stageFile(f, storage_service);
    // }

    /* It is necessary to store, or "stage", input files that only input. The getInputFiles()
     * method of the Workflow class returns the set of all workflow files that are not generated
     * by workflow tasks, and thus are only input files. These files are then staged on the storage service. */
    std::cerr << "Staging task input files..." << std::endl;
    for (auto const &f: workflow->getInputFiles()) {
        simulation->stageFile(f, storage_service);
    }


    cerr << "flop_rates " << simulation->getHostFlopRate("VirtualizedClusterProviderHost") << std::endl;
    cerr << "memory_capacity " << simulation->getHostMemoryCapacity("VirtualizedClusterProviderHost") << std::endl;
// *****************************************
    wms->myInfoSim = indoSimPointer;

// *****************************************
    std::cerr << "Launching the Simulation..." << std::endl;
    try {
        simulation->launch();
    } catch (std::runtime_error &e) {
        std::cerr << "Exception: " << e.what() << std::endl;
        throw;
    }
    std::cerr << "Simulation done!" << std::endl;



    
    SavedOrdonancement sav = SavedOrdonancement(wms->sheduledTaskList, wms->sheduledHostList, wms->generations, wms->nbFreeCorePerGeneration);
    // sav.print();


    /* Simulation results can be examined via simulation->getOutput(), which provides access to traces
     * of events. In the code below, we print the  retrieve the trace of all task completion events, print how
     * many such events there are, and print some information for the first such event. */
    auto trace = simulation->getOutput().getTrace<wrench::SimulationTimestampTaskCompletion>();
    for (auto const &item: trace) {
        std::cerr << "Task " << item->getContent()->getTask()->getID() << " completed at time " << item->getDate() << std::endl;
    }
    return sav;

}



//TODO changer les valeur pas defaut pour qu'elle soit générique
int LaunchDoubleSimulation(int argc, char **argv,
                           const string &saveOrdo1,
                           const string &saveOrdo2) {
    /* Parsing of the command-line arguments for this WRENCH simulation */
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " should only have the configuration file as an argument" << std::endl;
        exit(1);
    }
    ReadConfig config = ReadConfig(argv[1]);
    // config.saveConfig("/home/nanachibestwaifu/WORKDIR/wrench-2.1/examples/workflow_api/condor-grid-example/testOutputConfig.json");
    modifyArgv(argc, argv, config.workflowPath, config.platformPath);
    
        // add --log=custom_wms.threshold=info
        // Create a new argv array with additional argument
        // 
    //uncomment to activate logs
    // const int newArgc = argc + 1;
    // char* newArgv[newArgc + 1]; // +1 for the terminating nullptr
    // for (int i = 0; i < argc; ++i) {
    //     newArgv[i] = argv[i];
    // }
    // newArgv[argc] = strdup("--wrench-full-log");
    // newArgv[newArgc] = nullptr;

    // argv = newArgv;
    // argc = newArgc;



    //get a liste of hostes and their speed
    //TODO c'est ici qu'on chope la vitesse des hotes
    //TODO on peut sans doute rempplaceer par getHostnameList()
    listHS = getHostsFromXML(argv[1]);

    int pipefd[2];
    SavedOrdonancement savedOrdonan; // The value to be passed from child to parent
    string delimiter = "END_OF_THE_TRANSPHERED_STRING";

    // Create a pipe
    if (pipe(pipefd) == -1) {
        perror("pipe");
        exit(EXIT_FAILURE);
    }

    int c_pid = fork();
    int status;
  
    if (c_pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    }
    else if (c_pid > 0) {
         // wait(nullptr);
        std::cerr << "printed from parent process " << getpid() << std::endl;
        close(pipefd[1]); // Close the write end of the pipe
        waitpid(c_pid, &status, 0);
        std::cerr << "THE PARENT HAS WAITED " << std::endl;
        if (WIFSIGNALED(status)){
            std::cerr <<"Error befor the reading of the pipe \n";
        }
        else if (WEXITSTATUS(status)){
            std::cerr <<"Exited Normally\n";
        }


        std::string received_save = "";
        char buffer[256];  // Buffer to hold the read data
        ssize_t totalByteRead = 0;
        // Read from the pipe until reaching the end or a delimiter
        while (true) {
            // Read a chunk of data from the pipe
            ssize_t bytesRead = read(pipefd[0], buffer, sizeof(buffer) - 1);
            if (bytesRead <= 0) {
                break;  // End of input or error occurred
            }
            totalByteRead += bytesRead;

            // Null-terminate the buffer
            buffer[bytesRead] = '\0';

            received_save = received_save + buffer;

        }
        close(pipefd[0]); // Close the read end of the pipe
        
        std::cerr << "THERE WHERE " << totalByteRead << " READ" << std::endl;
        // std::cerr << "the received info was : " << received_save << std::endl;

        SavedOrdonancement save;
        save.deserialize(received_save);


        if (config.alea) {
            string newWFPath = "randomWorkflow.json";
            RandomWorkflowModification ran; 
            ran.setUpRandomnessParametres(config.randomMean,
                                          config.randomDeviation,
                                          config.seed);
            ran.randomizeWorklow(argv[2], newWFPath);

            // Convert std::string to non-const char*
            std::vector<char> charVec(newWFPath.begin(), newWFPath.end());
            charVec.push_back('\0');  // Add null terminator
            // char* charPtr = charVec.data();

            char* charPtr = new char[charVec.size()];  // Allocate memory using new operator
            std::copy(charVec.begin(), charVec.end(), charPtr);  // Copy vector elements to charPtr



            argv[2] = charPtr;
        }


        SavedOrdonancement newSave = launchSecondSimulation(argc, argv, save, config);
        // newSave.print();
        newSave.printToFile(saveOrdo2);
        exit(EXIT_SUCCESS);
    }
    else {
        std::cerr << "printed from child process " << getpid() << std::endl;
        close(pipefd[0]); // Close the read end of the pipe
        
        SavedOrdonancement save;

        // cerr << "this is not a test *************************************" << std::endl;




        save = lanchFirstSimulation(argc, argv, config);
        string data_to_send = save.serialize() ;//+ delimiter;
        save.printToFile(saveOrdo1);
        ssize_t bytes_written = write(pipefd[1], data_to_send.c_str(), data_to_send.size() * sizeof(char));
        if (bytes_written == -1) {
            perror("write");
            exit(EXIT_FAILURE);
        }
        cerr << "THERE WHERE " << bytes_written << " WRITTEN BYTES" << std::endl;
        // cerr << "the sent info was " << data_to_send << std::endl;
        // save.print();
        close(pipefd[1]); // Close the write end of the pipe
        std::cerr << "THE CHILD HAS FINISHED " << std::endl;


        exit(EXIT_SUCCESS);

    return 0;
    }
}
