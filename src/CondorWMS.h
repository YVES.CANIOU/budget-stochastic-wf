/**
 * Copyright (c) 2020. The WRENCH Team.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

#ifndef WRENCH_CONDORWMS_H
#define WRENCH_CONDORWMS_H

#include <wrench-dev.h>
#include "ReadConfig.h"
#include "infoSim.h"


namespace wrench {

    /**
     *  @brief A Workflow Management System (WMS) implementation
     */
    class CondorWMS : public ExecutionController {

    public:
        // Constructor
        CondorWMS(const std::shared_ptr<Workflow> &workflow,
                         // const std::shared_ptr<BatchComputeService> &batch_compute_service,
                         const std::shared_ptr<BatchComputeService> &batch_compute_service,
                         const std::shared_ptr<VirtualizedClusterComputeService> &virtualized_cluster_compute_service,
                         const std::shared_ptr<StorageService> &storage_service,
                         const std::string &hostname);

        std::vector<std::string> sheduledTaskList;//TODO passer ces variables dans une seul variable saveordo
        std::vector<std::string> sheduledHostList;
        std::vector<int> nbFreeCorePerGeneration;
        std::vector<int> generations;
        int currentGeneration = 0;

        void addConfig(ReadConfig &config);
        bool isConfig = false;
        ReadConfig *config;
        std::vector<std::tuple<std::string,double>> listHostSpeed;//liste of the hoste's name, it's vm name and computing service name and their speed in flops per seconde
        std::vector<std::tuple<std::string, double>> listCSNameSpeed;//liste assosciant le nom d'un computing service et sa vm

        std::shared_ptr<infoSim> myInfoSim;

    protected:
        double estimateTaskCompletionTime(std::shared_ptr<WorkflowTask> task, std::shared_ptr<BareMetalComputeService> compute_service);
        void addVmSpeed(const string &vmName, const string &vmComputingServiceName);

        void processEventStandardJobCompletion(std::shared_ptr<StandardJobCompletedEvent> event) override;
        void processEventStandardJobFailure(std::shared_ptr<StandardJobFailedEvent> event) override;
        void processEventPilotJobStart(std::shared_ptr<PilotJobStartedEvent> event) override;
        void processEventPilotJobExpiration(std::shared_ptr<PilotJobExpiredEvent> event) override;
        
    private:
        int main() override;
        /** @brief Whether the workflow execution should be aborted */
        bool abort = false;

        /** @brief A pilot job that is submitted to the batch compute service */
        std::shared_ptr<PilotJob> pilot_job = nullptr;
        /** @brief A boolean to indicate whether the pilot job is running */
        bool pilot_job_is_running = false;
        

        void scheduleReadyTasksGreedySimple(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);
        void scheduleReadyTasksRoundRobin(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);
        void scheduleReadyTasksReordonancer(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);
        void scheduleReadyTasksMinMin(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);
        void scheduleReadyTasksMTC(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);
        void scheduleReadyTasksHEFT(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);
        void scheduleReadyTasksMinMinBudg(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                std::shared_ptr<JobManager> job_manager,
                                std::set<std::shared_ptr<BareMetalComputeService>> compute_services);

        std::shared_ptr<Workflow> workflow;
        std::shared_ptr<BatchComputeService> batch_compute_service;
        std::shared_ptr<VirtualizedClusterComputeService> virtualized_cluster_compute_service;
        std::shared_ptr<StorageService> storage_service;
        
        std::map<std::shared_ptr<ComputeService>, unsigned long> core_utilization_map;
    
        std::shared_ptr<BareMetalComputeService> creatAndAddVM(int type, string vmName);
    };

}// namespace wrench

#endif//WRENCH_CONDORWMS_H
