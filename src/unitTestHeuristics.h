#ifndef UNIT_TEST_HEURISTICS_H
#define UNIT_TEST_HEURISTICS_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <gtest/gtest.h>
#include "CondorGridSimulator.h"

/**
 * @brief Compares two files line by line, ignoring empty lines and lines containing specified strings.
 *
 * @param file1         The path to the first file.
 * @param file2         The path to the second file.
 * @param ignoreStrings A vector of strings to ignore when comparing lines.
 *
 * @return `true` if the files are identical after ignoring empty lines and lines containing ignore strings,
 *         `false` otherwise.
 */
bool compareFiles(const std::string& file1, const std::string& file2, const std::vector<std::string>& ignoreStrings);

// Test fixture for the tools
class toolsTest : public ::testing::Test {
protected:
    void SetUp() override {
        // Set up any necessary objects or configurations before each test
    }

    void TearDown() override {
        // Clean up any resources allocated in SetUp()
    }
};

// Test fixture for the HeuristicsTest class
class HeuristicsTest : public ::testing::Test {
protected:
    void SetUp() override {
        // Set up any necessary objects or configurations before each test
    }

    void TearDown() override {
        // Clean up any resources allocated in SetUp()
    }
};

#endif // UNIT_TEST_HEURISTICS_H
