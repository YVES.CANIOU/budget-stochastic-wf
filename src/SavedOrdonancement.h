#ifndef SAVED_ORDONANCEMENT_H
#define SAVED_ORDONANCEMENT_H

#include <wrench-dev.h>
#include <fstream>  



class SavedOrdonancement {

	public :
    std::vector<std::string> sheduledTaskList;
    std::vector<std::string> sheduledHostList;
    std::vector<int> generations;
    std::vector<int> nbFreeCorePerGeneration;
    std::vector<bool> sheduled;
    long long int thisVariableIsATest = 42622222222;


    std::string serialize();
    void deserialize(std::string& str);

    /**
     * @brief      Constructs a new instance.
     */
    SavedOrdonancement();
    /**
     * @brief      Constructs a new instance.
     *
     * @param[in]  sheduledTaskList  The sheduled task list
     * @param[in]  sheduledHostList  The sheduled host list
     * @param[in]  generations       a generation is composed fo  all the tasks that where sheduled in the same call of scheduleReadyTasks
     */
    SavedOrdonancement(std::vector<std::string> sheduledTaskList,
						    std::vector<std::string> sheduledHostList,
    						std::vector<int> generations,
                            std::vector<int> nbFreeCorePerGeneration);

    void print();
    void printToFile(const string& path);

    /**
     * @brief      Finds a task id.
     *
     * @param[in]  HostName  The host name
     *
     * @return     the corresponding task id     */
    std::string findTaskID(std::string HostName);



    /**
     * @brief      Finds a host name.
     *
     * @param[in]  TaskID  The task id
     *
     * @return     the corresponding host  name 
     */
    std::string findHostName(std::string TaskID);



    /**
     * @brief      Gets the earliest non sheduled task for this vm.
     *
     * @param[in]  HostName  The virtual machine name
     *
     * @return     The taskID of the earliest non sheduled task for this vm.
     */
    std::string getEarliestNonSheduledTaskForThisVM(std::string HostName);



    /**
     * @brief      Sets the task as shceduled in the shceduled vector of this class.
     *
     * @param[in]  TaskID  The id of the task to set as shcelduled
     */
    void setTaskAsShceduled(std::string TaskID);






};

#endif //SAVED_ORDONANCEMENT_H