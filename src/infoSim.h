

#ifndef INFO_SIM_H
#define INFO_SIM_H

#include <wrench-dev.h>
#include <wrench.h>
#include <algorithm> // for std::sort
#include <map>       // for std::map
#include <tuple>     // for std::tuple
#include <vector>    // for std::vector
#include <string>
#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"
#include <set>
#include "ReadConfig.h"
#include <regex>
#include <iostream>
#include <fstream>
#include <sstream>


/**
 * @brief      This class contains inforamtion about the simulation that is used 
 * by heuristics (mainly) but is not disponible to them by via wrench. Handles the
 * distribution of hostNames so as to never use one that is already in use.
 */
class infoSim
{
public:
    infoSim();

    infoSim(std::shared_ptr<wrench::Simulation> sim,
               std::shared_ptr<wrench::Workflow> workflow,
               std::shared_ptr<wrench::SimpleStorageService> simpleStorageService,
               ReadConfig* readConfig);

    void setUp(std::shared_ptr<wrench::Simulation> sim);
    
    // static std::shared_ptr<infoSim> create(std::shared_ptr<wrench::Simulation> sim,
    //                                        std::shared_ptr<wrench::Workflow> workflow,
    //                                        std::shared_ptr<wrench::SimpleStorageService> simpleStorageService);


    //host names to ignoes when getting information on usable hosts
    const std::vector<std::string> hostNamesToIgnor = {"BatchNode", "VirtualizedClusterProviderHost", "WMSHost"};

    //la liste associant les nom des host et leur type.
    std::vector<std::tuple<std::string, int>> listHostNameAndSpeed;

    //une liste des hotes qui sont utilisable pour assigner des taches
    std::vector<std::tuple<std::string, int>> listUsableHostNameAndType;

    //liste of the bare metal services on which task can be attributed
    std::set<std::shared_ptr<wrench::BareMetalComputeService>> BareMetalList;

    //lists of all the hosts that already have a VM assigned to them
    std::vector<std::string> usedHosts;

    const long long int numberHostType = 3;
    std::vector<double> availableSpeed;

    bool isBandwidthsAndHostsSet = false;
    std::vector<std::tuple<std::string, std::string, std::string>>  bandwidthsAndHosts;
    double averageBandwidth = 0;
    double totalData = 0;


    bool isMaxTotalWorkSet = false;
    double maxTotalWork = 0;

    bool isNbMaxVMnSet = false;
    int nbMaxVMn = 0;



    shared_ptr<wrench::Workflow> workflow;
    shared_ptr<wrench::SimpleStorageService> simpleStorageService;
    shared_ptr<wrench::Simulation> simulation;
    ReadConfig* readConfig;




    // std::string getAvailableHoste(long long int type);

    /**
     * @brief      Sets the hostes.
     *
     * @param[in]  sim   the simulaiton
     */
    void setHostes(std::shared_ptr<wrench::Simulation> sim);

    /**
     * @brief      met les hots utilisable dans listUsableHostNameAndType
     */
    void setUsableHostes(std::shared_ptr<wrench::Simulation> sim);



    /**
     * @brief      Prints a host name and speed from the listHostNameAndSpeed 
     *              list.
     */
    void printHostNameAndSpeed() const;

    /**
     * @brief      Prints an host name and type of all the host to which a 
     *              workflow task can be attributed.
     */
    void printUsableHostNameAndType() const;
    /**
     * @brief      returns a vector of the hosts names to which tasks can be
     *             attributed
     *
     * @return     The usable hostes.
     */
    std::vector<std::string> getUsableHostes();

    /**
     * @brief      Gets a host that has never been used of type type.
     *
     * @param[in]  type  The type of the host
     *
     * @return     The name of the host.
     */
    std::string getHostOfType(int type);

    // function getBestHost(T, budgP T sk[T ], P, pot


    void extractBandwidthsAndHosts();
    void calculateAverageBandwidth();
    int CalculateNbMaxVMn();
    int getNbMaxVMn();
    double getMeanInitialCost();

    void printBandWidths();
    double getMeanSpeed();

    double getAverageBandwidth();
    double getMaxTotalTransfData();
    double getMaxTotalWork();
    double getBcalc();

    bool getPredSize(std::shared_ptr<wrench::WorkflowTask> task);

    bool isBudgPTskSet = false;
    unordered_map<std::string, double> budgPTsk;
    std::unordered_map<std::string, double> getDivBudget(double Bcalc);
    void divBudget(double Bcalc);
    double estimateTaskCompletionTime(std::shared_ptr<wrench::WorkflowTask> task, double speed);

    std::unordered_map<std::string, int> getBestHost(double Bcalc);



    bool isFileOutputOfSomeTask(const std::shared_ptr<wrench::DataFile>& file);
    std::vector<std::shared_ptr<wrench::WorkflowTask>> getTaskParents(const std::shared_ptr<wrench::WorkflowTask>& task);
    std::vector<std::shared_ptr<wrench::WorkflowTask>> getTaskChildren(const std::shared_ptr<wrench::WorkflowTask>& task);
    std::shared_ptr<wrench::WorkflowTask> getTaskByID(const std::string& id);



};

#endif//INFO_SIM_H
