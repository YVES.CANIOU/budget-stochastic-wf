#include "SavedOrdonancement.h"
#include "SecondSimulation.h"
#include "RandomWorkflowModification.h"
#include <iostream>
#include <wrench.h>
#include <wrench-dev.h>
#include <sys/wait.h>
#include "ReadConfig.h"
#include <vector>
#include <sstream>
#include <string>
#include <utility>
#include <algorithm>
#include <regex>

#include "CondorWMS.h"// WMS implementation
#include "RoundRobin.h"
#include "wrench/tools/wfcommons/WfCommonsWorkflowParser.h"



// SavedOrdonancement lanchFirstSimulation(int argc, char **argv, ReadConfig conf);
// SavedOrdonancement launchSecondSimulation(int argc, char **argv, SavedOrdonancement savedOrdonan, ReadConfig conf);
// void modifyArgv(int& argc, char* argv[], const std::string& platformPath, const std::string& workflowPath);
// static bool ends_with(const std::string &str, const std::string &suffix);
int LaunchDoubleSimulation(int argc, char **argv,
                           const string &saveOrdo1 = "/home/nanachibestwaifu/WORKDIR/ordoSaved1.txt",
                           const string &saveOrdo2 = "/home/nanachibestwaifu/WORKDIR/ordoSaved2.txt");