#include "ReadConfig.h"


// ReadConfig::ReadConfig(const string& configPath) {
//     ifstream configFile(configPath);
//     if (!configFile.is_open()) {
//         cout << "Error: Failed to open config file." << endl;
//         throw;
//     }

//     // Read the configuration values from the file
//     configFile >> this->configPath;
//     configFile >> workflowPath;
//     configFile >> platformPath;
//     configFile >> algo;
//     configFile >> seed;
//     configFile >> randomMean;
//     configFile >> randomDeviation;
//     configFile >> budget;

//     configFile.close();
// }


using json = nlohmann::json;

ReadConfig::ReadConfig(const std::string& filePath) {
  // Read the JSON file
  std::ifstream inputFile(filePath);
  if (!inputFile.is_open()) {
    std::cout << "Failed to open config file: " << filePath << std::endl;
    throw;
  }

  // Parse the JSON data
  json jsonData;
  try {
    inputFile >> jsonData;
  } catch (json::parse_error& e) {
    std::cout << "Failed to parse the config JSON file: " << e.what() << std::endl;
    inputFile.close();
    throw;
  }

  // Extract values from JSON
  try {
    this->configPath = jsonData["configPath"];
    workflowPath = jsonData["workflowPath"];
    platformPath = jsonData["platformPath"];
    algo = jsonData["algo"];
    seed = jsonData["seed"];
    alea = jsonData["alea"];
    randomMean = jsonData["randomMean"];
    useBudgetedHeuristic = jsonData["useBudgetedHeuristic"];
    randomDeviation = jsonData["randomDeviation"];
    budget = jsonData["budget"];


    for (const auto& value : jsonData["startUpTimeByType"]) {
      // startUpTimeByType.push_back(std::stod((std::string)value));
      startUpTimeByType.push_back((std::string)value);
    }
  } catch (json::type_error& e) {
    std::cout << "Failed to extract values from JSON: " << e.what() << std::endl;
    inputFile.close();
    throw;
  }

  inputFile.close();
}




// bool ReadConfig::saveConfig(const string& SavePath) {
//     ofstream configFile(SavePath);
//     if (!configFile.is_open()) {
//         cout << "Error: Failed to open config file for writing." << endl;
//         throw;
//     }

//     // Write the configuration values to the file
//     configFile << configPath << endl;
//     configFile << workflowPath << endl;
//     configFile << platformPath << endl;
//     configFile << algo << endl;
//     configFile << seed << endl;
//     configFile << randomMean << endl;
//     configFile << randomDeviation << endl;
//     configFile << budget << endl;

//     configFile.close();
//     return true;
// }

//TODO prendre en compte la startUpTimeByType
bool ReadConfig::saveConfig(const std::string& SavePath) {
  // Create a JSON object and assign values
  json jsonData;
  jsonData["configPath"] = configPath;
  jsonData["workflowPath"] = workflowPath;
  jsonData["platformPath"] = platformPath;
  jsonData["algo"] = algo;
  jsonData["seed"] = seed;
  jsonData["alea"] = alea;
  jsonData["randomMean"] = randomMean;
  jsonData["randomDeviation"] = randomDeviation;
  jsonData["budget"] = budget;
  jsonData["useBudgetedHeuristic"] = useBudgetedHeuristic;

  // Convert doubleList to a vector of strings
  // std::vector<std::string> doubleListStrings;
  // for (const auto& value : startUpTimeByType) {
  //   doubleListStrings.push_back(std::to_string(value));
  // }
  jsonData["startUpTimeByType"] = startUpTimeByType;
  // jsonData["startUpTimeByType"] = doubleListStrings;

  // Write the JSON data to the file
  std::ofstream outputFile(SavePath);
  if (!outputFile.is_open()) {
    std::cout << "Failed to open file for writing: " << SavePath << std::endl;
    return false;
  }

  try {
    outputFile << jsonData.dump(4);  // Indent JSON with 4 spaces for readability
  } catch (std::exception& e) {
    std::cout << "Failed to write JSON to file: " << e.what() << std::endl;
    outputFile.close();
    return false;
  }

  outputFile.close();
  return true;
}


