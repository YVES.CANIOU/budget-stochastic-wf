/**
 * Copyright (c) 2020. The WRENCH Team.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

#include <iostream>

#include "CondorWMS.h"
#include "CondorTimestamp.h"

constexpr double GB = 1000000000.0;


WRENCH_LOG_CATEGORY(custom_wms, "Log category for Custom WMS");



namespace wrench {

    void CondorWMS::addConfig(ReadConfig &config) {
        this->config = &config;
        isConfig = true;
    }


    CondorWMS::CondorWMS(const std::shared_ptr<Workflow> &workflow,
                     const std::shared_ptr<BatchComputeService> &batch_compute_service,
                     const std::shared_ptr<VirtualizedClusterComputeService> &virtualized_cluster_compute_service,
                     const std::shared_ptr<StorageService> &storage_service,
                     const std::string &hostname) :  ExecutionController(hostname, "simple"),
                                                     workflow(workflow),
                                                     batch_compute_service(batch_compute_service),
                                                     virtualized_cluster_compute_service(virtualized_cluster_compute_service),
                                                     storage_service(storage_service) {}


void printHostSpeed(const std::vector<std::tuple<std::string, double>>& listHostSpeed) {
    for (const auto& tuple : listHostSpeed) {
        std::cout << "Host ID: " << std::get<0>(tuple) << std::endl;
        std::cout << "Normalized Speed: " << std::get<1>(tuple) << std::endl;
        std::cout << "-----------------------" << std::endl;
    }
}

void CondorWMS::addVmSpeed(const string &vmName, const string &vmComputingServiceName) {

        std::vector<std::tuple<std::string, double>> vmSpeedList;

        // Get the computing service name from the tuple
        // std::string computingServiceName = std::get<2>(tuple);/////////////

        // Get the VM's host name using the computing service name
        // std::string vmHostName = cloud_compute_service->getVMPhysicalHostname(vmName);
        std::string vmHostName = virtualized_cluster_compute_service->getVMPhysicalHostname(vmName);

        // Find the speed associated with the VM's host in the listHostSpeed vector
        double speed = 0.0;
        for (const auto& hostTuple : listHostSpeed) {
            std::string hostName = std::get<0>(hostTuple);
            if (hostName == vmHostName) {
                speed = std::get<1>(hostTuple);
                break;
            }
        }

        // Create a tuple with the computing service name and speed
        std::tuple<std::string, double> tupleCSNameSpeed = std::make_tuple(vmComputingServiceName, speed);

        // Add the tuple to the vmSpeedList
        listCSNameSpeed.push_back(tupleCSNameSpeed);
}


void printListCSNameSpeed(const std::vector<std::tuple<std::string, double>>& listCSNameSpeed) {
    for (const auto& tuple : listCSNameSpeed) {
        std::cout << "Name: " << std::get<0>(tuple) << ", Speed: " << std::get<1>(tuple) << std::endl;
    }
}


/**
 * @brief      creat a VM, start it and add it to the available bare metal 
 * services for having task attributed to it.
 *
 * @param[in]  type    The type of host for the VM, determine it's speed. It
 * usualy goes up to 3, where 3 is the fastest.
 * 
 * @param[in]  vmName  The virtual Machine's name
 */
std::shared_ptr<BareMetalComputeService> CondorWMS::creatAndAddVM(int type, string vmName = "noNameSelected"){
    string hostName = myInfoSim->getHostOfType(type);
    string usedVmName;
    if (vmName == "noNameSelected") {
        usedVmName = hostName + "_VM";
    } else {
        usedVmName = vmName;
    }
    // std::cerr << "THE VM NAME IS  " << usedVmName << " THE HOSTNAME IS " << hostName << std::endl;
    std::string vm = this->virtualized_cluster_compute_service->createVM(1, 0.0,
            usedVmName,
            {{wrench::BareMetalComputeServiceProperty::THREAD_STARTUP_OVERHEAD,
            config->startUpTimeByType[type]}});
    //TODONOW rajouter le fait cette partie soit repliquer dans le reordonanceur 
    std::shared_ptr<BareMetalComputeService> vm_cs =
    this->virtualized_cluster_compute_service->startVM(vm, hostName);
    
    myInfoSim->BareMetalList.insert(vm_cs);
    addVmSpeed(vm, (*myInfoSim->BareMetalList.rbegin())->getName());
    this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

    return vm_cs;
}

/**
 * @brief main method of the CondorWMS daemon
 *
 * @return 0 on completion
 *
 * @throw std::runtime_error
 */
int CondorWMS::main() {
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);

    WRENCH_INFO("Starting on host %s", S4U_Simulation::getHostName().c_str());
    WRENCH_INFO("About to execute a workflow with %lu tasks", this->workflow->getNumberOfTasks());

    // Create a job manager
    auto job_manager = this->createJobManager();

    // Create a data movement manager
    auto data_movement_manager = this->createDataMovementManager();

    // std::set<std::shared_ptr<BareMetalComputeService>> BareMetalList;

    if (!config->useBudgetedHeuristic){
        std::cerr << "creating default VMs for a non budget based heuristic" << std::endl;

        creatAndAddVM(1, "VM1");
        creatAndAddVM(1, "VM2");
        creatAndAddVM(2, "VM3");
        creatAndAddVM(3, "VM4");

        // Create and start 4 VMs on the cloud service to use for the whole execution
        // auto vm1 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM1");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm1, "VirtualizedClusterHost1_1"));
        // addVmSpeed(vm1, (*myInfoSim->BareMetalList.rbegin())->getName());
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

        // auto vm2 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM2");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm2, "VirtualizedClusterHost1_2"));
        // addVmSpeed(vm2, (*myInfoSim->BareMetalList.rbegin())->getName());
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

        // auto vm3 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM3");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm3, "VirtualizedClusterHost2_1"));
        // addVmSpeed(vm3, (*myInfoSim->BareMetalList.rbegin())->getName());  
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

        // auto vm4 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM4");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm4, "VirtualizedClusterHost3_1"));
        // addVmSpeed(vm4, (*myInfoSim->BareMetalList.rbegin())->getName());
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;
    }

    // for (std::shared_ptr<BareMetalComputeService> bare : BareMetalList)
    //     cerr << "Nom Des Bare Metal Services " << bare->getName();

    while (true) {
        // If a pilot job is not running on the batch service, let's submit one that asks
        // for 3 cores on 2 compute nodes for 1 hour
        if (not pilot_job) {
            WRENCH_INFO("Creating and submitting a pilot job");
            pilot_job = job_manager->createPilotJob();
            job_manager->submitJob(pilot_job, this->batch_compute_service,
                                   {{"-N", "2"}, {"-c", "3"}, {"-t", "30"}});
        }

        // Construct the list of currently available bare-metal services (on VMs and perhaps within pilot job as well)
        std::set<std::shared_ptr<BareMetalComputeService>> available_compute_service = myInfoSim->BareMetalList;//{vm1_cs, vm2_cs, vm3_cs, vm4_cs};
        if (this->pilot_job_is_running) {
            // available_compute_service.insert(pilot_job->getComputeService());
        }

        /*HEURISTICS*/
        if (config->algo == "GreedySimple") {
            scheduleReadyTasksGreedySimple(workflow->getReadyTasks(), job_manager, available_compute_service);
        }
        else if (config->algo == "RoundRobin") {
            scheduleReadyTasksRoundRobin(workflow->getReadyTasks(), job_manager, available_compute_service);
        }
        else if (config->algo == "MinMin") {
            scheduleReadyTasksMinMin(workflow->getReadyTasks(), job_manager, available_compute_service);
        }
        else if (config->algo == "MTC") {
            scheduleReadyTasksMTC(workflow->getReadyTasks(), job_manager, available_compute_service);
        }
        else if (config->algo == "HEFT") {
            scheduleReadyTasksHEFT(workflow->getReadyTasks(), job_manager, available_compute_service);
        }
        else if (config->algo == "MinMinBudg"){
            scheduleReadyTasksMinMinBudg(workflow->getReadyTasks(), job_manager, available_compute_service);
        }
        else{
            cerr << "the heuristique algorithme " << config->algo << " specified in " << config->configPath << " does not exist" << std::endl;
            throw;
        }
        // Wait for a workflow execution event, and process it
        try {
            this->waitForAndProcessNextEvent();
        } catch (ExecutionException &e) {
            WRENCH_INFO("Error while getting next execution event (%s)... ignoring and trying again",
                        (e.getCause()->toString().c_str()));
            continue;
        }
        if (this->abort || this->workflow->isDone()) {
            break;
        }
    }

    // cerr << "THIS IS NOT A TEST" << endl;
    // printListCSNameSpeed(listCSNameSpeed);
    // cerr << endl << endl;
    // printListCSNameSpeed(listHostSpeed);
    // S4U_Simulation::sleep(10);

    WRENCH_INFO("--------------------------------------------------------");
    if (this->workflow->isDone()) {
        WRENCH_INFO("Workflow execution is complete!");
    } else {
        WRENCH_INFO("Workflow execution is incomplete!");
    }

    WRENCH_INFO("WMS terminating");

    return 0;
}

/**
 * @brief Process a StandardJobFailedEvent
 *
 * @param event: a workflow execution event
 */
void CondorWMS::processEventStandardJobFailure(std::shared_ptr<StandardJobFailedEvent> event) {
    auto job = event->standard_job;
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_RED);
    WRENCH_INFO("Task %s has failed", (*job->getTasks().begin())->getID().c_str());
    WRENCH_INFO("failure cause: %s", event->failure_cause->toString().c_str());
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
}

/**
* @brief Process a StandardJobCompletedEvent
*
* @param event: a workflow execution event
*/
void CondorWMS::processEventStandardJobCompletion(std::shared_ptr<StandardJobCompletedEvent> event) {
    auto job = event->standard_job;
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_BLUE);
    WRENCH_INFO("Task %s has COMPLETED (on service %s)",
                (*job->getTasks().begin())->getID().c_str(),
                job->getParentComputeService()->getName().c_str());
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
    this->core_utilization_map[job->getParentComputeService()]++;
}


/**
* @brief Process a PilotJobStartedEvent event
*
* @param event: a workflow execution event
*/
void CondorWMS::processEventPilotJobStart(std::shared_ptr<PilotJobStartedEvent> event) {
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_BLUE);
    WRENCH_INFO("The pilot job has started (it exposes bare-metal compute service %s)",
                event->pilot_job->getComputeService()->getName().c_str());
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
    this->pilot_job_is_running = true;
    this->core_utilization_map[this->pilot_job->getComputeService()] = event->pilot_job->getComputeService()->getTotalNumIdleCores();
}

/**
* @brief Process a processEventPilotJobExpiration even
*
* @param event: a workflow execution event
*/
void CondorWMS::processEventPilotJobExpiration(std::shared_ptr<PilotJobExpiredEvent> event) {
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_RED);
    WRENCH_INFO("The pilot job has expired (it was exposing bare-metal compute service %s)",
                event->pilot_job->getComputeService()->getName().c_str());
    TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);

    this->pilot_job_is_running = false;
    this->core_utilization_map.erase(this->pilot_job->getComputeService());
    this->pilot_job = nullptr;
}


/**
 * @brief Helper method to schedule a task one available compute services. This is a very, very
 *        simple/naive scheduling approach, that greedily runs tasks on idle cores of whatever
 *        compute services are available right now, using 1 core per task. Obviously, much more
 *        sophisticated approaches/algorithms are possible. But this is sufficient for the sake
 *        of an example.
 *
 * @param ready_task: the ready tasks to schedule
 * @param job_manager: a job manager
 * @param compute_services: available compute services
 * @return
 */
void CondorWMS::scheduleReadyTasksGreedySimple(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                   std::shared_ptr<JobManager> job_manager,
                                   std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {

    if (ready_tasks.empty()) {
        return;
    }

    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    unsigned long num_tasks_scheduled = 0;
    for (auto const &task: ready_tasks) {
        bool scheduled = false;
        for (auto const &cs: compute_services) {
            if (this->core_utilization_map[cs] > 0) {
                // Specify that ALL files are read/written from the one storage service
                std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
                for (auto const &f: task->getInputFiles()) {
                    file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
                }
                for (auto const &f: task->getOutputFiles()) {
                    file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
                }
                // file_locations[task->getInputFiles().at(0)] = FileLocation::LOCATION(storage_service);
                // file_locations[task->getOutputFiles().at(0)] = FileLocation::LOCATION(storage_service);

                try {
                    auto job = job_manager->createStandardJob(task, file_locations);
                    /** test*/
                    sheduledTaskList.push_back(task->getID());
                    int tmpNbCore = 0;
                    for (auto nbFreeCore : this->core_utilization_map)
                        tmpNbCore += nbFreeCore.second;
                    nbFreeCorePerGeneration.push_back(tmpNbCore);
                    sheduledHostList.push_back(cs->getName().c_str());
                    generations.push_back(currentGeneration);
                    /**     */
                    WRENCH_INFO(
                            "Submitting task %s to compute service %s", task->getID().c_str(),
                            cs->getName().c_str());
                    job_manager->submitJob(job, cs);
                    this->core_utilization_map[cs]--;
                    num_tasks_scheduled++;
                    scheduled = true;
                } catch (ExecutionException &e) {
                    WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                                "(I should get a notification of its expiration soon)",
                                task->getID().c_str());
                }
                break;
                
            }
        }
        if (not scheduled) break;
    }
    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}



void printList(const std::vector<std::tuple<std::string, double>>& list) {
    for (const auto& tuple : list) {
        std::cout << "Name: " << std::get<0>(tuple) << ", Speed: " << std::get<1>(tuple) << std::endl;
    }
}


/**
 * @brief Estimates the completion time of a task on a specific compute service.
 *
 * @param task              The task to estimate the completion time for
 * @param compute_service   The compute service to estimate the completion time on
 * @return The estimated completion time of the task on the compute service
 */
double CondorWMS::estimateTaskCompletionTime(std::shared_ptr<WorkflowTask> task, std::shared_ptr<BareMetalComputeService> compute_service) {
    // const double taskFlops = task->getFlops();
    // string hostName = cloud_compute_service->getVMPhysicalHostname(compute_service->getName());//->getPropertyValueAsDouble();//
    double hostSpeed = 0.0;
    for (const auto& host : listCSNameSpeed) {
        if (std::get<0>(host) == compute_service->getName()) {
            hostSpeed = std::get<1>(host);
        }
    }
    // return taskFlops / hostSpeed;
    return myInfoSim->estimateTaskCompletionTime(task, hostSpeed);
}
/**
 * @brief Method to schedule tasks using the MinMin task scheduling algorithm.
 *
 * @param ready_tasks     The ready tasks to schedule
 * @param job_manager     A job manager
 * @param compute_services    Available compute services
 */
void CondorWMS::scheduleReadyTasksMinMin(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                         std::shared_ptr<JobManager> job_manager,
                                         std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {
    if (ready_tasks.empty()) {
        return;
    }

    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    unsigned long num_tasks_scheduled = 0;
    for (auto const &task : ready_tasks) {
        std::shared_ptr<BareMetalComputeService> min_execution_cs;
        double min_execution_time = std::numeric_limits<double>::max();

        // Find the resource with the minimum execution time for the task
        for (auto const &cs : compute_services) {
            if (this->core_utilization_map[cs] > 0) {
                double execution_time = estimateTaskCompletionTime(task, cs);

                if (execution_time < min_execution_time) {
                    min_execution_time = execution_time;
                    min_execution_cs = cs;
                }
            }
        }//TODONOW factoriser en une fonction qui est aussi utiliser pour MinMinBudge et 

        if (min_execution_cs) {
            // Specify that ALL files are read/written from the one storage service
            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            for (auto const &f : task->getInputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            for (auto const &f : task->getOutputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            try {
                auto job = job_manager->createStandardJob(task, file_locations);
                sheduledTaskList.push_back(task->getID());
                int tmpNbCore = 0;
                for (auto nbFreeCore : this->core_utilization_map)
                    tmpNbCore += nbFreeCore.second;
                nbFreeCorePerGeneration.push_back(tmpNbCore);
                sheduledHostList.push_back(min_execution_cs->getName().c_str());
                generations.push_back(currentGeneration);

                WRENCH_INFO("Submitting task %s to compute service %s", task->getID().c_str(),
                            min_execution_cs->getName().c_str());
                job_manager->submitJob(job, min_execution_cs);
                this->core_utilization_map[min_execution_cs]--;
                num_tasks_scheduled++;
            } catch (ExecutionException &e) {
                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                            "(I should get a notification of its expiration soon)",
                            task->getID().c_str());
            }
        } else {
            WRENCH_INFO("No core available to have the task submitted to");
        }
    }

    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}





/**
 * @brief Method to schedule tasks using the MTC (Minimum Completion Time) task scheduling algorithm.
 *
 * @param ready_tasks     The ready tasks to schedule
 * @param job_manager     A job manager
 * @param compute_services    Available compute services
 */
void CondorWMS::scheduleReadyTasksMTC(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                      std::shared_ptr<JobManager> job_manager,
                                      std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {
    if (ready_tasks.empty()) {
        return;
    }

    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    unsigned long num_tasks_scheduled = 0;
    for (auto const &task : ready_tasks) {
        std::shared_ptr<BareMetalComputeService> mtc_cs;
        double min_completion_time = std::numeric_limits<double>::max();

        // Find the resource with the minimum completion time for the task
        for (auto const &cs : compute_services) {
            if (this->core_utilization_map[cs] > 0) {
                //TODO modifier pour que ce soit varaimmetn MTC, il devrais meme pas avoir de estimateTaskCompletionTime
                double completion_time = estimateTaskCompletionTime(task, cs);

                if (completion_time < min_completion_time) {
                    min_completion_time = completion_time;
                    mtc_cs = cs;
                }
            }
        }

        if (mtc_cs) {
            // Specify that ALL files are read/written from the one storage service
            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            for (auto const &f : task->getInputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            for (auto const &f : task->getOutputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            try {
                auto job = job_manager->createStandardJob(task, file_locations);
                sheduledTaskList.push_back(task->getID());
                int tmpNbCore = 0;
                for (auto nbFreeCore : this->core_utilization_map)
                    tmpNbCore += nbFreeCore.second;
                nbFreeCorePerGeneration.push_back(tmpNbCore);
                sheduledHostList.push_back(mtc_cs->getName().c_str());
                generations.push_back(currentGeneration);

                WRENCH_INFO("Submitting task %s to compute service %s", task->getID().c_str(),
                            mtc_cs->getName().c_str());
                job_manager->submitJob(job, mtc_cs);
                this->core_utilization_map[mtc_cs]--;
                num_tasks_scheduled++;
            } catch (ExecutionException &e) {
                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                            "(I should get a notification of its expiration soon)",
                            task->getID().c_str()); 
            }
        } else {
            WRENCH_INFO("No core available to have the task submitted to");
        }
    }

    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}


/**
 * @brief Calculates the rank of a task on a specific compute service.
 *
 * @param task  The task to calculate the rank for
 * @param cs    The compute service to calculate the rank on
 * @return The calculated rank
 */
double calculateTaskRank(const std::shared_ptr<WorkflowTask>& task, const std::shared_ptr<BareMetalComputeService>& cs) {
    // Some example factors used to calculate the rank
    double computation_cost = task->getFlops();
    //TODO ajouter le temps de calcules du transphere de donnees 
    // double data_transfer_cost = calculateDataTransferCost(task, cs);
    double rank = computation_cost ;//+ data_transfer_cost;
    return rank;
}




/**
 * @brief Method to schedule tasks using the Heterogeneous Earliest Finish Time (HEFT) task scheduling algorithm.
 *
 * @param ready_tasks     The ready tasks to schedule
 * @param job_manager     A job manager
 * @param compute_services    Available compute services
 */
void CondorWMS::scheduleReadyTasksHEFT(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                       std::shared_ptr<JobManager> job_manager,
                                       std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {
    if (ready_tasks.empty()) {
        return;
    }
    if (currentGeneration == 0)
    {
        cerr << "listCSNameSpeed" << std::endl;
        printList(listCSNameSpeed);
    }
    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    // Calculate the rank of each task on each compute service
    std::map<std::shared_ptr<WorkflowTask>, std::map<std::shared_ptr<BareMetalComputeService>, double>> task_ranks;
    for (auto const &task : ready_tasks) {
        for (auto const &cs : compute_services) {
            if (this->core_utilization_map[cs] > 0) {
                double rank = calculateTaskRank(task, cs);
                task_ranks[task][cs] = rank;
            }
        }
    }

    // Select the compute service with the highest rank for each task
    std::map<std::shared_ptr<WorkflowTask>, std::shared_ptr<BareMetalComputeService>> selected_services;
    for (auto const &task : ready_tasks) {
        double max_rank = 0;
        std::shared_ptr<BareMetalComputeService> max_rank_cs;

        for (auto const &cs : compute_services) {
            if (task_ranks[task][cs] > max_rank) {
                max_rank = task_ranks[task][cs];
                max_rank_cs = cs;
            }
        }

        selected_services[task] = max_rank_cs;
    }

    // Sort tasks based on their finish times in ascending order
    std::sort(ready_tasks.begin(), ready_tasks.end(), [&](const std::shared_ptr<WorkflowTask> &task1, const std::shared_ptr<WorkflowTask> &task2) {
        double finish_time1 = estimateTaskCompletionTime(task1, selected_services[task1]);
        double finish_time2 = estimateTaskCompletionTime(task2, selected_services[task2]);
        return finish_time1 < finish_time2;
    });

    // Schedule tasks in the sorted order
    unsigned long num_tasks_scheduled = 0;
    for (auto const &task : ready_tasks) {
        std::shared_ptr<BareMetalComputeService> heft_cs = selected_services[task];

        if (heft_cs) {
            // Specify that ALL files are read/written from the one storage service
            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            for (auto const &f : task->getInputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            for (auto const &f : task->getOutputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            try {
                auto job = job_manager->createStandardJob(task, file_locations);
                sheduledTaskList.push_back(task->getID());
                int tmpNbCore = 0;
                for (auto nbFreeCore : this->core_utilization_map)
                    tmpNbCore += nbFreeCore.second;
                nbFreeCorePerGeneration.push_back(tmpNbCore);
                sheduledHostList.push_back(heft_cs->getName().c_str());
                generations.push_back(currentGeneration);

                WRENCH_INFO("Submitting task %s to compute service %s", task->getID().c_str(),
                            heft_cs->getName().c_str());
                job_manager->submitJob(job, heft_cs);
                this->core_utilization_map[heft_cs]--;
                num_tasks_scheduled++;
            } catch (ExecutionException &e) {
                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                            "(I should get a notification of its expiration soon)",
                            task->getID().c_str());
            }
        } else {
            WRENCH_INFO("No core available to have the task submitted to");
        }
    }

    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}


   /**
 * @brief method to schedule tasks using round-robin task distribution.
 *
 * @param ready_tasks     The ready tasks to schedule
 * @param job_manager     A job manager
 * @param compute_services    Available compute services
 */
void CondorWMS::scheduleReadyTasksRoundRobin(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                    std::shared_ptr<JobManager> job_manager,
                                    std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {
    if (ready_tasks.empty()) {
        return;
    }

    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    unsigned long num_tasks_scheduled = 0;
    unsigned long cs_index = 0; // Index for round-robin distribution
    for (auto const &task : ready_tasks) {
        std::shared_ptr<BareMetalComputeService> cs = *std::next(compute_services.begin(), cs_index);

        if (this->core_utilization_map[cs] > 0) {
            // Specify that ALL files are read/written from the one storage service
            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            for (auto const &f : task->getInputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            for (auto const &f : task->getOutputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            try {
                auto job = job_manager->createStandardJob(task, file_locations);
                sheduledTaskList.push_back(task->getID());
                int tmpNbCore = 0;
                for (auto nbFreeCore : this->core_utilization_map)
                    tmpNbCore += nbFreeCore.second;
                nbFreeCorePerGeneration.push_back(tmpNbCore);
                sheduledHostList.push_back(cs->getName().c_str());
                generations.push_back(currentGeneration);

                WRENCH_INFO("Submitting task %s to compute service %s", task->getID().c_str(),
                            cs->getName().c_str());
                job_manager->submitJob(job, cs);
                this->core_utilization_map[cs]--;
                num_tasks_scheduled++;
            } catch (ExecutionException &e) {
                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                            "(I should get a notification of its expiration soon)",
                            task->getID().c_str());
            }
        } else {
            WRENCH_INFO("Compute service %s has no available cores for task scheduling", cs->getName().c_str());
        }
        cs_index = (cs_index + 1) % compute_services.size(); 
    }

    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}



void printBudgDivResults(unordered_map<std::string, double> budgPTsk) {
    double totalDudg = 0;
    for (const auto& pair : budgPTsk) {
        std::cout << pair.first << ": " << pair.second << std::endl;
        totalDudg += pair.second;
    }
    std::cout << "the total sum of the budgets is " << totalDudg;
}



/**
 * @brief      a simple class that is used to simplify the issue of handling a 
 * CS that could be an exiting VM or that could still be to be created
 */
struct InfoComputingService
{
public: 
    bool isNewVM = false;
    int type = 0;

    std::shared_ptr<BareMetalComputeService> existingCS;

    void set(std::shared_ptr<BareMetalComputeService> newPTR) {
        existingCS = newPTR;
        isNewVM =false;
    }

    void set(int type) {
        this->type = type;
        isNewVM = true;
    }

    void set(InfoComputingService ICS) {
        type = ICS.type;
        isNewVM = ICS.isNewVM;
        existingCS = ICS.existingCS;
    }

    void reset() {
        isNewVM = false;
        type = -1;

        existingCS.reset();
    }

};



/**
 * @brief method to schedule tasks using round-robin task distribution.
 *
 * @param ready_tasks     The ready tasks to schedule
 * @param job_manager     A job manager
 * @param compute_services    Available compute services
 */
/**
 * @brief method to schedule tasks using round-robin task distribution.
 *
 * @param ready_tasks         The ready tasks to schedule
 * @param job_manager         A job manager
 * @param compute_services    Available compute services
 */
void CondorWMS::scheduleReadyTasksMinMinBudg(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                            std::shared_ptr<JobManager> job_manager,
                                            std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {

    double Bclac = myInfoSim->getBcalc();
    InfoComputingService host;
    InfoComputingService selectedHost;
    unordered_map<std::string, double> budgPTsk = myInfoSim->getDivBudget(Bclac);

    // printBudgDivResults(budgPTsk);

    if (ready_tasks.empty()) {
        return;
    }

    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    unsigned long num_tasks_scheduled = 0;

    while (myInfoSim->workflow->getReadyTasks().size() > 0) {
        // trouver la tache avec le plus petit temspd'excution sir une machine donnee
        // std::shared_ptr<BareMetalComputeService> selectedHost;
        std::shared_ptr<WorkflowTask> selectedTask;
        double minFT = -1;

        // bool newVM = false;
        // int newVMType = 0;
        selectedHost.reset();
        host.reset();

        ready_tasks = myInfoSim->workflow->getReadyTasks();
        for (auto const &task : ready_tasks) {
            ///////////////////////////////////////////////////////////////
            /*   this part is an equivalent of the getBestHost function  */
            ///////////////////////////////////////////////////////////////
            std::shared_ptr<BareMetalComputeService> min_execution_cs;
            double min_execution_time = std::numeric_limits<double>::max();
            double BT = budgPTsk[task->getID()];/*+ pot;*///TODONOW 

            // Find the resource with the minimum execution time for the task
            for (auto const &cs : compute_services) {
                if (this->core_utilization_map[cs] > 0) { //TODONOW checker la difference de prix
                    double execution_time = estimateTaskCompletionTime(task, cs);

                    if (execution_time <= min_execution_time) {
                        min_execution_time = execution_time;
                        host.set(cs);
                        // cagnotteTmp = budgTsk - costTmp;//TODONOW
                    }
                }
            }

            int vmTypeCount = 1;
            // Find the resource with the minimum execution time for the task among potential new VMs
            for (auto const &speed : myInfoSim->availableSpeed) {
                double execution_time = myInfoSim->estimateTaskCompletionTime(task, speed);

                if (execution_time < min_execution_time) { //TODONOW checker la difference de prix
                    min_execution_time = execution_time;
                    host.set(vmTypeCount);
                    // cagnotteTmp = budgTsk - costTmp;//TODONOW
                }
                vmTypeCount++;
            }

            // find the resource with the minimum execution time for the task among the already used VMs
            // choper les anciennes VM avec myInfoSim->BareMetalList probablement

            // TODONOW mettre a jour la cagniotte on focntion de si on cree une VM ou non
            ///////////////////////////////////////////////////////////////

            // In the original paper The Erliest Computation Time would have been 
            // used as finishTime. But in our situation, the function 
            // scheduleReadyTasksMinMinBudg is only called at a 
            // given time of the execution, so all the factors that are not immediate 
            // file transfer and computation time of the task are already 
            // considered equal for the sake of this function.
            double finishTime = min_execution_time; //TODONOW calculate computation time

            if ((minFT < 0) || (finishTime < minFT)) {
                minFT = finishTime;
                selectedTask = task;
                selectedHost.set(host);
                //pot = newPot;//TODONOW 
            }
        }

        std::shared_ptr<BareMetalComputeService> selectedCS;


        if (selectedHost.isNewVM) {
            // create a new vm and set as the one to be used
            int type = selectedHost.type;
            selectedCS = creatAndAddVM(type);
        } else {
            selectedCS = selectedHost.existingCS;
        }

        if (selectedCS) {
            // Specify that ALL files are read/written from the one storage service
            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            for (auto const &f : selectedTask->getInputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            for (auto const &f : selectedTask->getOutputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            try {
                auto job = job_manager->createStandardJob(selectedTask, file_locations);
                sheduledTaskList.push_back(selectedTask->getID());
                int tmpNbCore = 0;
                for (auto nbFreeCore : this->core_utilization_map)
                    tmpNbCore += nbFreeCore.second;
                nbFreeCorePerGeneration.push_back(tmpNbCore);
                sheduledHostList.push_back(selectedCS->getName().c_str());
                generations.push_back(currentGeneration);

                WRENCH_INFO("Submitting task %s to compute service %s", selectedTask->getID().c_str(),
                            selectedCS->getName().c_str());
                job_manager->submitJob(job, selectedCS);
                this->core_utilization_map[selectedCS]--;
                num_tasks_scheduled++;
            } catch (ExecutionException &e) {
                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                            "(I should get a notification of its expiration soon)",
                            selectedTask->getID().c_str());
            }
        } else {
            WRENCH_INFO("No core available to have the task submitted to");
        }
    }

    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}



}// namespace wrench
