#include "RandomWorkflowModification.h" 














RandomWorkflowModification::RandomWorkflowModification(){

}
RandomWorkflowModification::RandomWorkflowModification(double mean, double standardDeviation, int seed = 0) {
	setUpRandomnessParametres( mean,  standardDeviation,  seed);
}




/**
 * @brief      set up the different parametres that are used for randomizing
 *             the workflow. Keep in mind that the randomly generated numbers are 
 *             multiplied to the makespan of the original workflow
 *
 * @param[in]  mean               The average value over a large nuber of generations
 * @param[in]  standardDeviation  The standard deviation
 * @param[in]  seed               The seed, 0 by default
 */
void RandomWorkflowModification::setUpRandomnessParametres(double mean, double standardDeviation, int seed) {
    has_been_set_up = true;


    this->seed = seed;
    this->mean = mean;
    this->standardDeviation = standardDeviation;

}


// void RandomWorkflowModification::exportModification();
void RandomWorkflowModification::randomizeWorklow(const string& pathOriginalWF, const string& pathNewWF) {
	if (!has_been_set_up) {
		cerr << "the worklowRandomizer has not been set up, use the setUpRandomnessParametres member function" << std::endl;
		throw;
	}


    // List of strings to search for in each line
    std::vector<std::string> searchStrings = {"\"runtime\": ", "\"avgCPU\": "};
    // Create a GSL random number generator
    const gsl_rng_type* rng_type = gsl_rng_default;
    gsl_rng* rng = gsl_rng_alloc(rng_type);

    std::ifstream inputFile(pathOriginalWF);
    std::string content((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
    inputFile.close();

    for (const std::string& searchString : searchStrings) {
        std::size_t pos = 0;

        while ((pos = content.find(searchString, pos)) != std::string::npos) {
            pos += searchString.length();
            std::size_t commaPos = content.find(',', pos);

            if (commaPos != std::string::npos) {
                std::string value = content.substr(pos, commaPos - pos);
                try {
                    double randomValue = gsl_ran_gaussian(rng, standardDeviation) + mean;
                    if (randomValue < 0)
                        randomValue = -randomValue;

                    double currentValue = std::stod(value);
                    double newValue = currentValue * randomValue;
                    content.replace(pos, commaPos - pos, std::to_string(newValue));
                    pos += std::to_string(newValue).length();
                } catch (const std::invalid_argument& e) {
                    // Ignore and continue processing next occurrence
                }
            }
        }
    }

    // Create a GSL random number generator
    // const gsl_rng_type* rng_type = gsl_rng_default;
    // gsl_rng* rng = gsl_rng_alloc(rng_type);

    // std::ifstream inputFile(pathOriginalWF);
    // std::string content((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
    // inputFile.close();

    // std::string searchString = "\"runtime\": ";
    // std::size_t pos = 0;

    // while ((pos = content.find(searchString, pos)) != std::string::npos) {
    //     pos += searchString.length();
    //     std::size_t commaPos = content.find(',', pos);

    //     if (commaPos != std::string::npos) {
    //         std::string makespanValue = content.substr(pos, commaPos - pos);
    //         try {
	// 			double randomValue = gsl_ran_gaussian(rng, standardDeviation) + mean;
	// 			if (randomValue < 0)
	// 				randomValue = -randomValue;

    //             double makespan = std::stod(makespanValue);
    //             double newMakespan = makespan * randomValue;
    //             content.replace(pos, commaPos - pos, std::to_string(newMakespan));
    //             pos += std::to_string(newMakespan).length();
    //         } catch (const std::invalid_argument& e) {
    //             // Ignore and continue processing next makespan occurrence
    //         }
    //     }
    // }

    std::ofstream outputFile(pathNewWF);
    if (outputFile) {
        outputFile << content;
        std::cout << "File copied and makespan values multiplied successfully." << std::endl;
    } else {
        std::cout << "Failed to open the destination file for writing." << std::endl;
    }

    outputFile.close();
    // Free the GSL random number generator
    gsl_rng_free(rng);
}












    // std::ifstream originalFile(pathOriginalWF, std::ios::binary);
    // std::ofstream newFile(pathNewWF, std::ios::binary);

    // if (originalFile) {
    //     newFile << originalFile.rdbuf();
    // } else {
    //     std::cout << "Failed to open the source file." << std::endl;
    // }

    // inputFile.close();
    // outputFile.close();


    // std::string searchString = "\"makespan\": ";
    // std::size_t pos = 0;
    
    // while ((pos = content.find(searchString, pos)) != std::string::npos) {
    //     pos += searchString.length();
    //     std::size_t commaPos = content.find(',', pos);
        
    //     if (commaPos != std::string::npos) {
    //         std::string makespanValue = content.substr(pos, commaPos - pos);
    //         int makespan = std::stoi(makespanValue);
    //         int newMakespan = makespan * multiplier;
    //         content.replace(pos, commaPos - pos, std::to_string(newMakespan));
    //         pos += std::to_string(newMakespan).length();
    //     }
    // }



    // double randomValue = gsl_ran_gaussian(rng, sigma) + mean;
    // #include <iostream>

// // void replaceMakespan(const std::string& fileName, int multiplier) {
//     std::ifstream inputFile(pathNewWF);
//     std::string line;
//     std::string modifiedContent;
    
//     while (std::getline(inputFile, line)) {
//         size_t makespanPos = line.find("\"makespan\": ");
//         if (makespanPos != std::string::npos) {
//             size_t valuePos = makespanPos + 12; // Position after "makespan": 
//             size_t commaPos = line.find(',', valuePos);
//             if (commaPos != std::string::npos) {
//                 std::string makespanValue = line.substr(valuePos, commaPos - valuePos);
//                 int makespan = std::stoi(makespanValue);
//                 int newMakespan = makespan * multiplier;
//                 std::string newLine = line.substr(0, valuePos) + std::to_string(newMakespan) + line.substr(commaPos);
//                 modifiedContent += newLine + "\n";
//             } else {
//                 modifiedContent += line + "\n";
//             }
//         } else {
//             modifiedContent += line + "\n";
//         }
//     }

//     inputFile.close();

//     std::ofstream outputFile(fileName);
//     if (outputFile) {
//         outputFile << modifiedContent;
//         std::cout << "Makespan values replaced successfully." << std::endl;
//     } else {
//         std::cout << "Failed to open the file for writing." << std::endl;
//     }

//     outputFile.close();
// }

// int main() {
//     std::string fileName = "data.json"; // Replace with the actual file name
//     int multiplier = 5; // Replace with the desired multiplier value

//     replaceMakespan(fileName, multiplier);

//     return 0;
// }


// }
// void RandomWorkflowModification::randomizeWorklowFromExistingModification();//name pending