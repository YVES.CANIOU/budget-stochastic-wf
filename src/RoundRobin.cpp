/**
 * Copyright (c) 2020. The WRENCH Team.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

#include <iostream>

#include "RoundRobin.h"
#include "CondorTimestamp.h"

constexpr double GB = 1000000000.0;


WRENCH_LOG_CATEGORY(custom_wms_RoundRobin, "Log category for Custom WMS");


//
namespace wrench {

    // RoundRobin::RoundRobin(const std::shared_ptr<Workflow> &workflow,
    //           // const std::shared_ptr<wrench::BatchComputeService> &batch_compute_service,
    //           const std::shared_ptr<VirtualizedClusterComputeService> &virtualized_cluster_compute_service,
    //           const std::shared_ptr<wrench::StorageService> &storage_service,//may be part of the virtualized service
    //           std::string hostname) : ExecutionController(hostname, "condor-grid"),
    //                                   workflow(workflow),
    //                                   virtualized_cluster_compute_service(virtualized_cluster_compute_service),
    //                                   storage_service(storage_service)
    //             {}
    RoundRobin::RoundRobin(const std::shared_ptr<Workflow> &workflow,
                     const std::shared_ptr<BatchComputeService> &batch_compute_service,
                     const std::shared_ptr<CloudComputeService> &cloud_compute_service,
                     const std::shared_ptr<StorageService> &storage_service,
                     const std::string &hostname) : ExecutionController(hostname, "simple"),
                                                    workflow(workflow),
                                                    batch_compute_service(batch_compute_service),
                                                    cloud_compute_service(cloud_compute_service),
                                                    storage_service(storage_service) {}


    /**
     * @brief main method of the RoundRobin daemon
     *
     * @return 0 on completion
     *
     * @throw std::runtime_error
     */
    int RoundRobin::main() 
    {

        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);


        WRENCH_INFO("Starting on host %s", S4U_Simulation::getHostName().c_str());
        WRENCH_INFO("About to execute a workflow with %lu tasks", this->workflow->getNumberOfTasks());

        /* Create a job manager so that we can create/submit jobs */
        auto job_manager = this->createJobManager();

        // Create a data movement manager
        auto data_movement_manager = this->createDataMovementManager();

        /*CREATION DES VM */
        // Create and start two VMs on the cloud service to use for the whole execution
        auto vm1 = this->cloud_compute_service->createVM(2, 0.0);// 2 cores, 0 RAM (RAM isn't used in this simulation)
        auto vm1_cs = this->cloud_compute_service->startVM(vm1);
        this->core_utilization_map[vm1_cs] = 2;

        auto vm2 = this->cloud_compute_service->createVM(4, 0.0);// 4 cores, 0 RAM (RAM isn't used in this simulation)
        auto vm2_cs = this->cloud_compute_service->startVM(vm2);
        this->core_utilization_map[vm2_cs] = 4;

        // /* Create a VM instance with 5 cores and one with 2 cores (and 500M of RAM) */
        // WRENCH_INFO("Creating a 'large' VM with 5 cores  and a 'small' VM with 2 cores, both of them with 5GB RAM");
        // auto large_vm = virtualized_cluster_compute_service->createVM(5, 5 * GB);
        // auto small_vm = virtualized_cluster_compute_service->createVM(2, 5 * GB);


        /*LANCER LES VM CREE A LA MAIN OU PAR XML*/

        // /* Start the VMs */
        // WRENCH_INFO("Start the large VM on host VirtualizedClusterHost1");
        // auto large_vm_compute_service = virtualized_cluster_compute_service->startVM(large_vm, "VirtualizedClusterHost1");
        // WRENCH_INFO("Start the small VM on host VirtualizedClusterHost2");
        // auto small_vm_compute_service = virtualized_cluster_compute_service->startVM(small_vm, "VirtualizedClusterHost2");

        while (true) {

         // while (not this->workflow->isDone()) {
            // If a pilot job is not running on the batch service, let's submit one that asks
            // for 3 cores on 2 compute nodes for 1 hour
            if (not pilot_job) {
                WRENCH_INFO("Creating and submitting a pilot job");
                pilot_job = job_manager->createPilotJob();
                job_manager->submitJob(pilot_job, this->batch_compute_service,
                                       {{"-N", "2"}, {"-c", "3"}, {"-t", "800000"}});
            }
            /* Get the ready tasks */
            // auto ready_tasks = this->workflow->getReadyTasks();


            // Construct the list of currently available bare-metal services (on VMs and perhaps within pilot job as well)
            std::set<std::shared_ptr<BareMetalComputeService>> available_compute_service = {vm1_cs, vm2_cs};
            
            if (this->pilot_job_is_running) {
                /*peut etre pas a jouter dans la version final, je pense que ca peut rajouter un outils de calcule en plus de ce
                 * qu'il y a dans le xml
                 */
                // available_compute_service.insert(pilot_job->getComputeService());
            }
            
            /*HEURISTICS*/
            scheduleReadyTasks(workflow->getReadyTasks(), job_manager, available_compute_service);

            // Wait for a workflow execution event, and process it
            try {
                this->waitForAndProcessNextEvent();
            } 
            catch (ExecutionException &e) {
                WRENCH_INFO("Error while getting next execution event (%s)... ignoring and trying again",
                            (e.getCause()->toString().c_str()));
                continue;
            }
            if (this->abort || this->workflow->isDone()) {
                break;
            }
            /*auto standard_job1 = job_manager->createStandardJob(cheap_ready_task, file_locations1);*/

            /*job_manager->submitJob(BEST_JOB, VM_SELECTED);*/

            /* Sort them by flops */
            // std::sort(ready_tasks.begin(), ready_tasks.end(),
            //           [](const std::shared_ptr<WorkflowTask> &t1, const std::shared_ptr<WorkflowTask> &t2) -> bool {
            //               if (t1->getFlops() == t2->getFlops()) {
            //                   return ((uintptr_t) t1.get() > (uintptr_t) t2.get());
            //               } else {
            //                   return (t1->getFlops() < t2->getFlops());
            //               }
            //           });

            /*  Pick the least and most expensive task */
            // auto cheap_ready_task = ready_tasks.at(0);
            // auto expensive_ready_task = ready_tasks.at(ready_tasks.size() - 1);

            // std::cerr << "ceci est un test ************************************* " << std::endl;

            // /* Submit the cheap task to the small VM */
            // /* First, we need to create a map of file locations, stating for each file
            //  * where is should be read/written */
            // /*TROUVEE POURQUOI IL ARRIVE PAS A TROUVEE LE FICHIER*/
            // std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations1;
            // file_locations1[cheap_ready_task->getInputFiles().at(0)] = FileLocation::LOCATION(storage_service);
            // file_locations1[cheap_ready_task->getOutputFiles().at(0)] = FileLocation::LOCATION(storage_service);


            // std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            // for (auto const &f: cheap_ready_task->getInputFiles()) {
            //     file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            //     std::cerr << "file location input " << file_locations[f]->getFullAbsolutePath() << std::endl;
            // }
            // for (auto const &f: cheap_ready_task->getOutputFiles()) {
            //     file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            //     std::cerr << "file location output " << file_locations[f]->getFullAbsolutePath() << std::endl;
            // }
            
            
            
            





            // /* Create the job  */
            // WRENCH_INFO("Creating a job to run task %s (%.2lf)",
            //             cheap_ready_task->getID().c_str(), cheap_ready_task->getFlops());

            // auto standard_job1 = job_manager->createStandardJob(cheap_ready_task, file_locations1);


            /*test prints*/
            // std::cerr << std::endl << "scratch : " << standard_job1->usesScratch() << std::endl;
            // std::cerr << "getMinimumRequiredNumCores : " << standard_job1->getMinimumRequiredNumCores() << std::endl;
            // std::cerr << "getMinimumRequiredMemory : " << standard_job1->getMinimumRequiredMemory() << std::endl;
            // // std::cerr << "getTasks : " << standard_job1->getTasks() << std::endl;
            // std::cerr << "getNumCompletedTasks : " << standard_job1->getNumCompletedTasks() << std::endl;
            // std::cerr << "getNumTasks : " << standard_job1->getNumTasks() << std::endl;
            // std::cerr << "getState : " << standard_job1->getState() << std::endl;



            // /* Submit the job to the small VM */
            // WRENCH_INFO("Submit this job to the large_vm_compute_service VM");
            // job_manager->submitJob(standard_job1, large_vm_compute_service);

            // /* Submit the expensive task to the large VM */
            // /* First, we need to create a map of file locations, stating for each file
            //  * where is should be read/written */
            // std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations2;
            // file_locations2[expensive_ready_task->getInputFiles().at(0)] = FileLocation::LOCATION(storage_service);
            // file_locations2[expensive_ready_task->getOutputFiles().at(0)] = FileLocation::LOCATION(storage_service);

            // /* Create the job  */
            // WRENCH_INFO("Creating a job to run task %s (%.2lf)",
            //             expensive_ready_task->getID().c_str(), expensive_ready_task->getFlops());

            // auto standard_job2 = job_manager->createStandardJob(expensive_ready_task, file_locations2);

            // /* Submit the job to the large VM */
            // WRENCH_INFO("Submit this job to the large VM");
            // job_manager->submitJob(standard_job2, large_vm_compute_service);




            /*ON ATTEND QUE LES TACHES SE TERMINES ET QU ELLE EXECUTE LEUR EVENEMETN DE FIN, A FAIRE AUTANT 
             *DE FOIS QU'IL Y EUT DE TACHES LANCER*/
            WRENCH_INFO("Wait for next event");
            this->waitForAndProcessNextEvent();

         }
        S4U_Simulation::sleep(10);

        WRENCH_INFO("--------------------------------------------------------");
        if (this->workflow->isDone()) {
            WRENCH_INFO("Workflow execution is complete!");
        } else {
            WRENCH_INFO("Workflow execution is incomplete!");
        }

        WRENCH_INFO("WMS terminating");
        return 0;
    }



    // void RoundRobin::processEventStandardJobCompletion(std::shared_ptr<StandardJobCompletedEvent> event) 
    // {
    //     /* Retrieve the job that this event is for */
    //     auto job = event->standard_job;
    //     /* Retrieve the job's task */
    //     for(long unsigned int i = 0; i < job->getNumTasks(); i++) {
    //         auto task = job->getTasks().at(i);
    //         WRENCH_INFO("Notified that a standard job has completed task %s", task->getID().c_str());
    //     }
    
    /**
     * @brief Process a StandardJobFailedEvent
     *
     * @param event: a workflow execution event
     */
    void RoundRobin::processEventStandardJobFailure(std::shared_ptr<StandardJobFailedEvent> event) {
        auto job = event->standard_job;
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_RED);
        WRENCH_INFO("Task %s has failed", (*job->getTasks().begin())->getID().c_str());
        WRENCH_INFO("failure cause: %s", event->failure_cause->toString().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
    }


    
    // void RoundRobin::processEventStandardJobFailure(std::shared_ptr<StandardJobFailedEvent> event) 
    // {
    //     /* Retrieve the job that this event is for */
    //     auto job = event->standard_job;
    //     /* Retrieve the job's task */
    //     for(long unsigned int i = 0; i < job->getNumTasks(); i++) {
    //         auto task = job->getTasks().at(i);
    //         // WRENCH_INFO("Notified that a standard job has failed for task %s with error %s",
    //         //             task->getID().c_str(),
    //         //             event->failure_cause->toString().c_str());
    //         std::cerr << "Notified that a standard job has failed for task " <<  task->getID().c_str() << " with error " << event->failure_cause->toString().c_str() << std::endl;
    //     }
    //     /* Print some error message */
    //     throw std::runtime_error("ABORTING DUE TO JOB FAILURE");
    // }


    /**
    * @brief Process a StandardJobCompletedEvent
    *
    * @param event: a workflow execution event
    */
    void RoundRobin::processEventStandardJobCompletion(std::shared_ptr<StandardJobCompletedEvent> event) {
        auto job = event->standard_job;
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_BLUE);
        WRENCH_INFO("Task %s has COMPLETED (on service %s)",
                    (*job->getTasks().begin())->getID().c_str(),
                    job->getParentComputeService()->getName().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
        this->core_utilization_map[job->getParentComputeService()]++;
    }

    /**
    * @brief Process a PilotJobStartedEvent event
    *
    * @param event: a workflow execution event
    */
    void RoundRobin::processEventPilotJobStart(std::shared_ptr<PilotJobStartedEvent> event) {
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_BLUE);
        WRENCH_INFO("The pilot job has started (it exposes bare-metal compute service %s)",
                    event->pilot_job->getComputeService()->getName().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
        this->pilot_job_is_running = true;
        this->core_utilization_map[this->pilot_job->getComputeService()] = event->pilot_job->getComputeService()->getTotalNumIdleCores();
    }
        /**
    * @brief Process a processEventPilotJobExpiration even
    *
    * @param event: a workflow execution event
    */
    void RoundRobin::processEventPilotJobExpiration(std::shared_ptr<PilotJobExpiredEvent> event) {
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_RED);
        WRENCH_INFO("The pilot job has expired (it was exposing bare-metal compute service %s)",
                    event->pilot_job->getComputeService()->getName().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);

        this->pilot_job_is_running = false;
        this->core_utilization_map.erase(this->pilot_job->getComputeService());
        this->pilot_job = nullptr;
    }


   /**
 * @brief method to schedule tasks using round-robin task distribution.
 *
 * @param ready_tasks     The ready tasks to schedule
 * @param job_manager     A job manager
 * @param compute_services    Available compute services
 */
void RoundRobin::scheduleReadyTasks(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                    std::shared_ptr<JobManager> job_manager,
                                    std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {
    if (ready_tasks.empty()) {
        return;
    }

    WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());

    unsigned long num_tasks_scheduled = 0;
    unsigned long cs_index = 0; // Index for round-robin distribution
    for (auto const &task : ready_tasks) {
        std::shared_ptr<BareMetalComputeService> cs = *std::next(compute_services.begin(), cs_index);

        if (this->core_utilization_map[cs] > 0) {
            // Specify that ALL files are read/written from the one storage service
            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
            for (auto const &f : task->getInputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            for (auto const &f : task->getOutputFiles()) {
                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
            }
            try {
                auto job = job_manager->createStandardJob(task, file_locations);
                sheduledTaskList.push_back(task->getID());
                sheduledHostList.push_back(cs->getName().c_str());
                generations.push_back(currentGeneration);

                WRENCH_INFO("Submitting task %s to compute service %s", task->getID().c_str(),
                            cs->getName().c_str());
                job_manager->submitJob(job, cs);
                this->core_utilization_map[cs]--;
                num_tasks_scheduled++;
            } catch (ExecutionException &e) {
                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                            "(I should get a notification of its expiration soon)",
                            task->getID().c_str());
            }
        } else {
            WRENCH_INFO("Compute service %s has no available cores for task scheduling", cs->getName().c_str());
        }
        cs_index = (cs_index + 1) % compute_services.size(); 
    }

    currentGeneration++;
    WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());
}


}// namespace wrench
