#ifndef READ_CONFIG_H
#define READ_CONFIG_H

#include <wrench-dev.h>
#include <fstream>  
#include <iostream>
#include <fstream>
#include <string>
#include <nlohmann/json.hpp>

using namespace std;

/*
 * the config file is under the following format 
 * 	the wokflow path 
 * 	the platform path
 * 	the seed used to randomise the time needed for a task to finish
 * 	the budget 
 * 
 */

class ReadConfig
{
public:

    
    string configPath;
    string workflowPath;
    string platformPath;
    string algo;
    bool alea = true;
    int seed = 0;
    double randomMean = 0;
    double randomDeviation = 0;
    int budget = 0;
    bool useBudgetedHeuristic = false;
    std::vector<std::string> startUpTimeByType;
    ReadConfig();
/**
 * @brief      Reads a configuration from a config file.
 *
 * @param[in]  configPath  The path the configuration si read from
 * @param      workflow    The variable to save the workflow at
 * @param      platform    The variable to save the The platform at
 * @param      seed        The variable to save the The seed at
 * @param      budget      The variable to save the The budget at
 *
 */
    ReadConfig(const string& configPath);
    
/**
 * @brief Saves a configuration to a file in the specified format.
 *
 * @param[in] configPath The path to the config file.
 * @param[in] workflow   The workflow path to be saved.
 * @param[in] platform   The platform path to be saved.
 * @param[in] seed       The seed to be saved.
 * @param[in] budget     The budget to be saved.
 *
 * @return True if the configuration is successfully saved, false otherwise.
 */
bool saveConfig(const string& SavePath);


};







#endif//READ_CONFIG_H