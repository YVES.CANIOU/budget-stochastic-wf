#ifndef RANDOM_WORKFLOW_MODIFICATION_H
#define RANDOM_WORKFLOW_MODIFICATION_H

#include <wrench-dev.h>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <stdexcept>


/**
 * @brief      This class is used to generate a new workflow from an existing one. The new worklfow has it's
 * makespans valeus randomized following the parameters that can be set up in setUpRandomnessParametres
 */
class RandomWorkflowModification {
public:
	string path_to_old_workflow;
	string path_to_new_workflow;
	vector<float> list_of_coeficients;




	RandomWorkflowModification();
	RandomWorkflowModification(double mean, double standardDeviation, int seed);



	/**
	 * @brief      set up the different parametres that are used for randomizing
	 *             the workflow. Keep in mind that the randomly generated numbers are 
	 *             multiplied to the makespan of the original workflow
	 *
	 * @param[in]  mean               The average value over a large nuber of generations
	 * @param[in]  standardDeviation  The standard deviation
	 * @param[in]  seed               The seed, 0 by default
	 */
	void setUpRandomnessParametres(double mean, double standardDeviation, int seed);
	/**
	 * @brief      save the coeficients in a file if you need to test a workflow with the same values
	 */
	void exportModification();
	/**
	 * @brief      creat a new workflow with randomized values. they are
	 *             modified by a multiplication with a random coeficient
	 *
	 * @param[in]  pathOriginalWF  The path original workflow
	 * @param[in]  pathNewWF       The path new workflow
	 */
	void randomizeWorklow(const string& pathOriginalWF, const string& pathNewWF);
	/**
	 * @brief      creat a new workflow with using values that were saved via the exportModification methode.
	 */
	void randomizeWorklowFromExistingModification();//name pending

private:
    int seed;
    double mean;
    double standardDeviation;
    float has_been_set_up = false;
};











#endif //RANDOM_WORKFLOW_MODIFICATION_H