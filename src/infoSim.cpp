#include "infoSim.h"




infoSim::infoSim(){

};

infoSim::infoSim(std::shared_ptr<wrench::Simulation> sim,
                 std::shared_ptr<wrench::Workflow> workflow,
                 std::shared_ptr<wrench::SimpleStorageService> simpleStorageService,
                 ReadConfig* readConfig) {
	setUsableHostes(sim);
	setHostes(sim);

    this->simulation = sim;
    this->workflow = workflow;
    this->simpleStorageService = simpleStorageService;
    this->readConfig = readConfig;
	// populateSpeedVector();
}

// static std::shared_ptr<infoSim> infoSim::create(std::shared_ptr<wrench::Simulation> sim,
//                                        std::shared_ptr<wrench::Workflow> workflow,
//                                        std::shared_ptr<wrench::SimpleStorageService> simpleStorageService) {
//     return std::shared_ptr<infoSim>(new infoSim(sim, workflow, simpleStorageService, simulation));
// }

void infoSim::setUp(std::shared_ptr<wrench::Simulation> sim){
	setUsableHostes(sim);
	setHostes(sim);

    //TODO clacultate mean speed
}

/**
 * @brief      Sets the hosts.
 *
 * @param[in]  sim   the simulation
 */
void infoSim::setHostes(std::shared_ptr<wrench::Simulation> sim) {
    std::vector<std::string> hostNameList = sim->getHostnameList();

    // Populate the vector with host names and speeds
    for (const std::string& hostName : hostNameList) {
        long long int speed = sim->getHostFlopRate(hostName);
        listHostNameAndSpeed.emplace_back(hostName, speed);
    }
}


/**
 * @brief      Checks if a string contains a string from a given list of strings.
 *
 * @param[in]  inputString  The input string to check.
 * @param[in]  stringList   The list of strings to search in.
 *
 * @return     True if the input string contains any string from the list, false otherwise.
 */
bool stringContainsStringFromList(const std::string& inputString, const std::vector<std::string>& stringList) {
    for (const std::string& str : stringList) {
        if (inputString.find(str) != std::string::npos) {
            return true;
        }
    }
    return false;
}


/**
 * @brief      met les hots utilisable dans listUsableHostNameAndType
 */
void infoSim::setUsableHostes(std::shared_ptr<wrench::Simulation> sim){
    std::vector<std::string> hostNameList = sim->getHostnameList();
    std::map<int, int> speedCounts;

    // Count the frequency of each speed
    for (const std::string& hostName : hostNameList) {
        long long int speed = sim->getHostFlopRate(hostName);
        speedCounts[speed]++;
    }

    // Sort the speeds by their counts in descending order
    std::vector<std::pair<int, int>> sortedSpeeds(speedCounts.begin(), speedCounts.end());
    std::sort(sortedSpeeds.begin(), sortedSpeeds.end(),
              [](const std::pair<int, int>& a, const std::pair<int, int>& b) {
                  return a.second > b.second;
              });

    // Create a new list of the most common host speeds and their ranks
    // std::vector<std::tuple<std::string, int>> mostCommonHostSpeeds;
    for (long long int i = 0; i < numberHostType; ++i) {
        long long int speed = sortedSpeeds[i].first;
        long long int rank = i + 1;
        for (const std::string& hostName : hostNameList) {
            if (sim->getHostFlopRate(hostName) == speed &&
            	!stringContainsStringFromList(hostName, hostNamesToIgnor)) {
                listUsableHostNameAndType.emplace_back(hostName, rank);
            }
        }
        availableSpeed.emplace_back(speed);
    }


    //sorts the array which contains the differents speed available
    std::sort(availableSpeed.begin(), availableSpeed.end(),
          [](long long int a, long long int b) {
              return a > b;
          });

    // for (long long int a : availableSpeed)
    // 	cerr << "******************* " << a << std::endl;
}


// /**
//  * @brief      creats a table with the differente speeds of the usable hosts 
//  * ranked from slowest to fastest
//  */
// void infoSim::populateSpeedVector() {
//         std::vector<int> uniqueSpeeds;

//         // Extract unique speeds from the list
//         for (const auto& tuple : listUsableHostNameAndType) {
//             long long int speed = std::get<1>(tuple);
//             if (std::find(uniqueSpeeds.begin(), uniqueSpeeds.end(), speed) == uniqueSpeeds.end()) {
//                 uniqueSpeeds.push_back(speed);
//             }
//         }

//         // Sort the speeds from slowest to fastest
//         std::sort(uniqueSpeeds.begin(), uniqueSpeeds.end());

//         // Update the speed vector
//         speed = uniqueSpeeds;

//         for (long long int tmp : speed)
//         {
//         	cerr << "********************* " << tmp << std::endl; 
//         }
//     }



/**
 * @brief      Prints the list of host names and speeds.
 */
void infoSim::printHostNameAndSpeed() const {
    std::cout << "List of Host Names and Speeds:" << std::endl;
    for (const auto& tuple : listHostNameAndSpeed) {
        std::string hostName = std::get<0>(tuple);
        long long int speed = std::get<1>(tuple);
        std::cout << "Host Name: " << hostName << ", Speed: " << speed << std::endl;
    }
    std::cout << std::endl;
}

/**
 * @brief      Prints the list of usable host names and types.
 */
void infoSim::printUsableHostNameAndType() const {
    std::cout << "List of Usable Host Names and Types:" << std::endl;
    for (const auto& tuple : listUsableHostNameAndType) {
        std::string hostName = std::get<0>(tuple);
        long long int type = std::get<1>(tuple);
        std::cout << "Host Name: " << hostName << ", Type: " << type << std::endl;
    }
    std::cout << std::endl;
}

/**
 * @brief      returns a vector of the hosts names to which tasks can be
 *             attributed
 *
 * @return     The usable hostes.
 */
std::vector<std::string> infoSim::getUsableHostes()
{
    std::vector<std::string> hostNames;

    for (const auto& tuple : listUsableHostNameAndType) {
        std::string hostName = std::get<0>(tuple);
        hostNames.push_back(hostName);
    }

    return hostNames;
}


/**
 * @brief      Gets a host that has never been used of type type.
 *
 * @param[in]  type  The type of the host
 *
 * @return     The name of the host.
 */
std::string infoSim::getHostOfType(int type) {
    std::string host;

    // Search for the first host with the given type and not in usedHosts
    for (const auto& tuple : listUsableHostNameAndType) {
        const std::string& hostName = std::get<0>(tuple);
        int hostType = std::get<1>(tuple);

        // std::cerr << "nom de l'hote " << std::get<0>(tuple) << " type demander " << type << " type trouvée " << hostType << std::endl;

        // Check if the host is of the correct type and not in usedHosts
        if (hostType == type && std::find(usedHosts.begin(), usedHosts.end(), hostName) == usedHosts.end()) {
            host = hostName;
            usedHosts.push_back(host);
            break;
        }
    }

    return host;
}



void printWorkflowTasks(const std::map<std::string, std::shared_ptr<wrench::WorkflowTask>>& tasks) {
    for (const auto& pair : tasks) {
        std::cout << "Key: " << pair.first << ", Name of the task: " << pair.second->getFlops() << std::endl;
    }
}


double infoSim::getAverageBandwidth() {
    if (!isBandwidthsAndHostsSet) {
        extractBandwidthsAndHosts();
        calculateAverageBandwidth();
        // std::cerr << "CECI N'EST PAS UN TEST" << std::endl;
        // std::cerr << "averageBandwidth " << averageBandwidth << std::endl;
        // printBandWidths();

        isBandwidthsAndHostsSet = true;
    }
    return averageBandwidth;
}


/**
 * @brief      Gets the maximum total transf data required to execute the workflow
 *
 * @return     The maximum total transf data required to execute the workflow
 */
double infoSim::getMaxTotalTransfData() {
    double maxTotalTransfData = 0.0;
    
    // Get the list of exit tasks
    std::map<std::string, std::shared_ptr<wrench::WorkflowTask>> exitTasks = workflow->getExitTaskMap();

    // Iterate over the exit tasks
    for (const auto& exitTaskPair : exitTasks) {
        std::shared_ptr<wrench::WorkflowTask> exitTask = exitTaskPair.second;

        // Get the list of parents for the exit task
        std::vector<std::shared_ptr<wrench::WorkflowTask>> parents = workflow->getTaskParents(exitTask);

        // Iterate over the parents and calculate the total transfer data
        double totalTransfData = 0.0;
        for (const auto& parent : parents) {
            // Check if the parent task is an output file
            if (workflow->isFileOutputOfSomeTask(parent->getOutputFiles()[0])) {
                // Calculate the transfer data based on the file size
                double fileSize = parent->getOutputFiles()[0]->getSize();
                totalTransfData += fileSize;
            }
        }

        // Update the maximum total transfer data if necessary
        if (totalTransfData > maxTotalTransfData) {
            maxTotalTransfData = totalTransfData;
        }
    }

    return maxTotalTransfData;
}

std::shared_ptr<wrench::WorkflowTask> infoSim::getTaskByID(const std::string& id) {
    std::map<std::string, std::shared_ptr<wrench::WorkflowTask>> taskMap = workflow->getTaskMap();
    auto it = taskMap.find(id);
    if (it != taskMap.end()) {
        return it->second;
    } else {
        throw std::invalid_argument("Task not found with ID: " + id);
    }
}

bool infoSim::isFileOutputOfSomeTask(const std::shared_ptr<wrench::DataFile>& file) {
    std::map<std::string, std::shared_ptr<wrench::WorkflowTask>> taskMap = workflow->getTaskMap();

    for (const auto& taskPair : taskMap) {
        std::shared_ptr<wrench::WorkflowTask> task = taskPair.second;
        // if (task->isOutputData(file)) {
        //     return true;
        // }
    }

    return false;
}

std::vector<std::shared_ptr<wrench::WorkflowTask>> infoSim::getTaskParents(const std::shared_ptr<wrench::WorkflowTask>& task) {
    std::vector<std::shared_ptr<wrench::WorkflowTask>> parents;
    std::map<std::string, std::shared_ptr<wrench::WorkflowTask>> taskMap = workflow->getTaskMap();

    for (const auto& taskPair : taskMap) {
        std::shared_ptr<wrench::WorkflowTask> currentTask = taskPair.second;
        // if (currentTask != task && currentTask->isOutputData(task->getInputData())) {
        //     parents.push_back(currentTask);
        // }
    }

    return parents;
}

std::vector<std::shared_ptr<wrench::WorkflowTask>> infoSim::getTaskChildren(const std::shared_ptr<wrench::WorkflowTask>& task) {
    std::vector<std::shared_ptr<wrench::WorkflowTask>> children;
    std::map<std::string, std::shared_ptr<wrench::WorkflowTask>> taskMap = workflow->getTaskMap();

    for (const auto& taskPair : taskMap) {
        std::shared_ptr<wrench::WorkflowTask> currentTask = taskPair.second;
        // if (currentTask != task && task->isOutputData(currentTask->getInputData())) {
        //     children.push_back(currentTask);
        // }
    }

    return children;
}

double infoSim::getMaxTotalWork() {
    if (!isMaxTotalWorkSet) {
        std::vector<std::shared_ptr<wrench::WorkflowTask>> taskVect = workflow->getTasks();
        maxTotalWork = wrench::Workflow::getSumFlops(taskVect);
        std::cerr << "maxTotalWork " << maxTotalWork << std::endl;

        isMaxTotalWorkSet = true;
    }
    return maxTotalWork;
}


double infoSim::getMeanSpeed() {
    double count = 0;
    double acc = 0;
    for (int speed : availableSpeed) {
        acc += speed;
        count++;
    }
    if (count > 0)
        return acc / count;
    return 0.0;
}   

bool infoSim::getPredSize(std::shared_ptr<wrench::WorkflowTask> task) {
    std::vector<std::shared_ptr<wrench::WorkflowTask>> predlist = 
    workflow->getTaskParents(task);
    return wrench::Workflow::getSumFlops(predlist);
}

int infoSim::CalculateNbMaxVMn() {
  int res = 0;
  std::map<std::shared_ptr<wrench::WorkflowTask>, char> mapVu;

  // Initialize the map: all tasks are set to 0
  for (const auto& task : workflow->getTasks()) {
    mapVu[task] = 0;
  }

  // Calculate the maximum number of VMs
  res = 0;
  for (const auto& task : workflow->getTasks()) {
    if (task->getFlops() > 0) {
      res++;
    }
  }

  // res -= 2;

  return res;
}

int infoSim::getNbMaxVMn() {
    if (!isNbMaxVMnSet) {
        nbMaxVMn = CalculateNbMaxVMn();
        isNbMaxVMnSet = true;
    }
    return nbMaxVMn;
}

double infoSim::getMeanInitialCost() {
    double i = 0;
    double totalInitCost = 0;
    for (auto initCostStr : readConfig->startUpTimeByType) {
        totalInitCost += std::stod(initCostStr);
        i++;
    }
    if (i == 0)
        return 0;
    return totalInitCost / i;
}

    // const double Bcalc = budget - (nbVMsMax * iniCost_m + cagnotte);
double infoSim::getBcalc() {
    double meanInitialCost = getMeanInitialCost();
    double NbMaxVMn = (double)(getNbMaxVMn());
    double cagnotte = 0;//TOODNOW

    return readConfig->budget - (getNbMaxVMn() * meanInitialCost + cagnotte);
}


/**
 * @brief      creats a table of estimated budgets 
 *
 * @param[in]  workflow   The workflow
 * @param[in]  Bcalc      The total budget after the cost of the cloud storage
 *                        and VM utilisation has be substracted.
 * @param[in]  meanSpeed  The mean speed of the VMs
 * @param[in]  bw         the band width between a VM and the cloud Storage
 *
 * @return     the budget associated with every task
 */
void infoSim::divBudget(double Bcalc) {

    double totalTransherData = getMaxTotalTransfData();
    double totalWork = getMaxTotalWork();
    double meanSpeed = getMeanSpeed();
    double averageBandwidth = getAverageBandwidth();
    std::map<std::string, std::shared_ptr<wrench::WorkflowTask>>
    taskMap =  workflow->getTaskMap();

    budgPTsk.clear();
    // unordered_map<std::string, double> budgPTsk;
    for (const auto& pair : taskMap) {
        const std::string& key = pair.first;
        const std::shared_ptr<wrench::WorkflowTask>& task = pair.second;
        
        //on considere Wi cette valeurs comme la moyenne des valeurs
        //  possibles pour cette tache
        double Wt= task->getFlops();
        //estimation de la variation maximum de la tache, car on fait une
        // estimation consevatrice
        double Wt_teta = Wt + Wt * readConfig->randomDeviation;
        //caluler la taille des predeceseurs de la tache t
        double size_Dpred_t = getPredSize(task);

        //calcule du budget de la tache
        budgPTsk[task->getID()] = Bcalc * ((Wt_teta / meanSpeed) +
                                 (size_Dpred_t / averageBandwidth)) / 
                                 ((totalWork / meanSpeed) +
                                 (totalTransherData / averageBandwidth));
    }
    // return budgPTsk;
}

std::unordered_map<std::string, double> infoSim::getDivBudget(double Bcalc) {
    if (isBudgPTskSet) {
        divBudget(Bcalc);
        isBudgPTskSet = true;
    }
    return budgPTsk;
}


//TODO deplacer estimateTaskCompletionTime et getBestHost dans le fichier des heuristiques

/**
 * @brief Estimates the completion time of a task on a specific compute service.
 *
 * @param task              The task to estimate the completion time for
 * @param compute_service   The compute service to estimate the completion time on
 * @return The estimated completion time of the task on the compute service
 */
double infoSim::estimateTaskCompletionTime(std::shared_ptr<wrench::WorkflowTask> task, double speed) {
    const double taskFlops = task->getFlops();

    double computationTime = taskFlops / speed;

    // Compute the data transfer time for the task 
    double dataSize = 0.0;
    const auto& inputFiles = task->getInputFiles();
    for (const auto& file : inputFiles) {
        dataSize += file->getSize();
    }
    //we use the average bandwidth because all the VMs that matter have the same
    double dataTransferTime = dataSize / getAverageBandwidth();

    return computationTime + dataTransferTime;
}



// using namespace std;
// using namespace rapidxml;

 
// void infoSim::extractBandwidthsAndHosts() {
//     std::string filename = readConfig->platformPath;

//     file<> xmlFile(filename.c_str());
//     xml_document<> doc;
//     doc.parse<0>(xmlFile.data());

//     xml_node<>* root = doc.first_node();
//     xml_node<>* zoneNode = root->first_node("zone");
//     while (zoneNode) {
//         for (xml_node<>* node = zoneNode->first_node(); node; node = node->next_sibling()) {
//             if (std::string(node->name()) == "link") {
//                 std::string id = node->first_attribute("id")->value();
//                 std::string bandwidth = node->first_attribute("bandwidth")->value();
//                 std::string host1, host2;

//                 xml_node<>* routeNode = zoneNode->first_node("route");
//                 while (routeNode) {
//                     std::string src = routeNode->first_attribute("src")->value();
//                     std::string dst = routeNode->first_attribute("dst")->value();
//                     if (id == src) {
//                         host1 = dst;
//                     } else if (id == dst) {
//                         host2 = src;
//                     }
//                     routeNode = routeNode->next_sibling("route");
//                 }

//                 if (!host1.empty() && !host2.empty()) {
//                     bandwidthsAndHosts.push_back(std::make_tuple(host1, host2, bandwidth));
//                 }
//             }
//         }
//         zoneNode = zoneNode->next_sibling("zone");
//     }
// }

// void infoSim::extractBandwidthsAndHosts() {
//     std::string filename = readConfig->platformPath;
//     // std::vector<std::tuple<std::string, std::string, std::string>> bandwidthsAndHosts;
//     std::map<std::string, std::string> linkBandwidths;

//     tinyxml2::XMLDocument doc;
//     if (doc.LoadFile(filename.c_str()) != tinyxml2::XML_SUCCESS) {
//         throw std::runtime_error("Failed to load XML file.");
//     }

//     tinyxml2::XMLElement* platformElem = doc.FirstChildElement("platform");
//     if (!platformElem) {
//         throw std::runtime_error("Invalid XML format. <platform> element not found.");
//     }

//     // Extract link names and their associated bandwidths
//     for (tinyxml2::XMLElement* linkElem = platformElem->FirstChildElement("link"); linkElem;
//          linkElem = linkElem->NextSiblingElement("link")) {
//         const char* linkId = linkElem->Attribute("id");
//         const char* bandwidth = linkElem->Attribute("bandwidth");
//         if (linkId && bandwidth) {
//             linkBandwidths[linkId] = bandwidth;
//         }
//     }


void infoSim::extractBandwidthsAndHosts() {
    std::string filename = readConfig->platformPath;
    std::ifstream file(filename);
    std::string line;
    std::string xmlContent;
    while (std::getline(file, line)) {
        xmlContent += line;
    }
    file.close();

    std::regex linkRegex("<link\\s+id=\"(\\w+)\"\\s+bandwidth=\"(\\w+)\"");
    std::regex routeRegex("<route\\s+src=\"(\\w+)\"\\s+dst=\"(\\w+)\">\\s*<link_ctn\\s+id=\"(\\w+)\"/>\\s*</route>");

    std::smatch match;

    // Extract link names and their associated bandwidths
    auto linkIt = std::sregex_iterator(xmlContent.begin(), xmlContent.end(), linkRegex);
    auto linkEnd = std::sregex_iterator();
    std::vector<std::tuple<std::string, std::string>> linkBandwidths;
    for (; linkIt != linkEnd; ++linkIt) {
        std::string linkName = (*linkIt)[1].str();
        std::string bandwidth = (*linkIt)[2].str();
        linkBandwidths.push_back(std::make_tuple(linkName, bandwidth));
    }

    // Extract hosts and their associated bandwidths
    auto routeIt = std::sregex_iterator(xmlContent.begin(), xmlContent.end(), routeRegex);
    auto routeEnd = std::sregex_iterator();
    for (; routeIt != routeEnd; ++routeIt) {
        std::string srcHost = (*routeIt)[1].str();
        std::string dstHost = (*routeIt)[2].str();
        std::string linkName = (*routeIt)[3].str();

        // Find the bandwidth for the current link
        std::string bandwidth;
        for (const auto& link : linkBandwidths) {
            if (std::get<0>(link) == linkName) {
                bandwidth = std::get<1>(link);
                break;
            }
        }

        if (!bandwidth.empty()) {
            bandwidthsAndHosts.push_back(std::make_tuple(srcHost, dstHost, bandwidth));
        }
    }

    // return bandwidthsAndHosts;
}


double convertBandwidthToMbps(const std::string& bandwidth) {
    std::string valueStr = bandwidth;
    std::string unit;

    // Extract numerical value and unit from the bandwidth string
    std::smatch match;
    std::regex regex("(\\d+(?:\\.\\d+)?)\\s*(\\w+)");
    if (std::regex_match(bandwidth, match, regex)) {
        valueStr = match[1].str();
        unit = match[2].str();
    }

    double value = std::stod(valueStr);

    // Convert the value to Mbps based on the unit
    if (unit == "EBps") {
        value *= 8 * std::pow(1024, 3);  // Exabyte to Megabit
    } else if (unit == "TBps") {
        value *= 8 * std::pow(1024, 2);  // Terabyte to Megabit
    } else if (unit == "GBps") {
        value *= 8 * 1024;  // Gigabyte to Megabit
       } else if (unit == "MBps") {
        value *= 8;  // Megabyte to Megabit
    } else if (unit == "KBps") {
        value /= 128;  // Kilobyte to Megabit
    }

    return value;
}

void infoSim::calculateAverageBandwidth() {
    double totalBandwidth = 0.0;
    int count = 0;

    for (const auto& data : bandwidthsAndHosts) {
        std::string srcHost = std::get<0>(data);
        std::string dstHost = std::get<1>(data);
        std::string bandwidth = std::get<2>(data);

        if ((srcHost == "WMSHost" || dstHost == "WMSHost") && 
            (srcHost != "WMSHost" || dstHost != "WMSHost")) {
            double convertedBandwidth = convertBandwidthToMbps(bandwidth);
            // std::cerr << "bandwidth of one tuple " << convertedBandwidth << std::endl;
            totalBandwidth += convertedBandwidth;
            count++;
        }
    }

    if (count > 0) {

        averageBandwidth = totalBandwidth / count;
    }
    else {
        averageBandwidth = 0.0;
    }

}




/**
 * @brief      Print the extracted bandwidths and hosts

 */
void infoSim::printBandWidths() {
    for (const auto& link : bandwidthsAndHosts) {
        std::cout << "Host 1: " << std::get<0>(link) << ", Host 2: " << std::get<1>(link) << ", Bandwidth: " << std::get<2>(link) << std::endl;
    }
}



// void infoSim::calculateAverageBandwidth() {
    // std::vector<double> bandwidths;
    // for (const auto& entry : bandwidthsAndHosts) {
    //     const std::string& bandwidth = entry.first;
    //     const std::string& hostName = entry.second;
    //     if (!stringContainsStringFromList(hostName, hostNamesToIgnor)) {
    //         bandwidths.push_back(std::stod(bandwidth));
    //     }
    // }
    // if (bandwidths.empty()) {
    //     averageBandwidth = 0.0;
    // }
    // double sum = 0.0;
    // for (double bandwidth : bandwidths) {
    //     sum += bandwidth;
    // }
    // averageBandwidth = sum / bandwidths.size();
// }