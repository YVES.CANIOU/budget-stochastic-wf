/**
 * Copyright (c) 2020. The WRENCH Team.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 */

#include <iostream>

#include "SecondSimulation.h"
#include "CondorTimestamp.h"

constexpr double GB = 1000000000.0;


WRENCH_LOG_CATEGORY(custom_wms2, "Log category for Custom WMS");

void CetteFonctionEstUnTest()
{
    std::cerr << "cette fonciton est un test" << std::endl;
}




namespace wrench {

    // SecondeSimulation::SecondeSimulation(const std::shared_ptr<Workflow> &workflow,
    //           // const std::shared_ptr<wrench::BatchComputeService> &batch_compute_service,
    //           const std::shared_ptr<VirtualizedClusterComputeService> &virtualized_cluster_compute_service,
    //           const std::shared_ptr<wrench::StorageService> &storage_service,//may be part of the virtualized service
    //           std::string hostname) : ExecutionController(hostname, "condor-grid"),
    //                                   workflow(workflow),
    //                                   virtualized_cluster_compute_service(virtualized_cluster_compute_service),
    //                                   storage_service(storage_service)
    //             {}
    SecondeSimulation::SecondeSimulation(const std::shared_ptr<Workflow> &workflow,
                     const std::shared_ptr<BatchComputeService> &batch_compute_service,
                     const std::shared_ptr<StorageService> &storage_service,
                     const std::shared_ptr<VirtualizedClusterComputeService> &virtualized_cluster_compute_service,
                     const std::string &hostname) : ExecutionController(hostname, "simple"),
                                                    workflow(workflow),
                                                    batch_compute_service(batch_compute_service),
                                                    storage_service(storage_service),
                                                    virtualized_cluster_compute_service(virtualized_cluster_compute_service) {}


    /**
     * @brief      Adds the ordonancement, it should be simply a part of the contructor but i could not manage to .
     *
     * @param[in]  savOrdoIn  The saved ordonancement in
     */
    void SecondeSimulation::addOrdo(SavedOrdonancement savOrdoIn) {
        savOrdo = savOrdoIn;
    }




    /**
     * @brief      Adds a configuration.
     *
     * @param      config  The configuration
     */
    void SecondeSimulation::addConfig(ReadConfig &config) {
        this->config = &config;
    }



    /**
     * @brief      creat a VM, start it and add it to the available bare metal 
     * services for having task attributed to it.
     *
     * @param[in]  type    The type of host for the VM, determine it's speed. It
     * usualy goes up to 3, where 3 is the fastest.
     * 
     * @param[in]  vmName  The virtual Machine's name
     */
    void SecondeSimulation::creatAndAddVM(int type, const string& vmName){
            auto vm = this->virtualized_cluster_compute_service->createVM(1, 0.0, vmName);
            string hostName = myInfoSim->getHostOfType(type);
            myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm, hostName));
            // addVmSpeed(vm, (*myInfoSim->BareMetalList.rbegin())->getName());
            this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;
    }


    /**
     * @brief main method of the SecondeSimulation daemon
     *
     * @return 0 on completion
     *
     * @throw std::runtime_error
     */
    int SecondeSimulation::main() 
    {

        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);


        // WRENCH_INFO("WMS starting on host %s", Simulation::getHostName().c_str());
        // WRENCH_INFO("About to execute a workflow with %lu tasks", this->workflow->getNumberOfTasks());

        WRENCH_INFO("Starting on host %s", S4U_Simulation::getHostName().c_str());
        WRENCH_INFO("About to execute a workflow with %lu tasks", this->workflow->getNumberOfTasks());

        /* Create a job manager so that we can create/submit jobs */
        auto job_manager = this->createJobManager();

        // Create a data movement manager
        auto data_movement_manager = this->createDataMovementManager();




    if (!config->useBudgetedHeuristic){

        creatAndAddVM(1, "VM1");
        creatAndAddVM(1, "VM2");
        creatAndAddVM(2, "VM3");
        creatAndAddVM(3, "VM4");
        
        // // Create and start 4 VMs on the cloud service to use for the whole execution
        // auto vm1 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM1");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm1, "VirtualizedClusterHost1_1"));
        // // addVmSpeed(vm1, (*myInfoSim->BareMetalList.rbegin())->getName());
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

        // auto vm2 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM2");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm2, "VirtualizedClusterHost1_2"));
        // // addVmSpeed(vm2, (*myInfoSim->BareMetalList.rbegin())->getName());
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

        // auto vm3 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM3");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm3, "VirtualizedClusterHost2_1"));
        // // addVmSpeed(vm3, (*myInfoSim->BareMetalList.rbegin())->getName());  
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;

        // auto vm4 = this->virtualized_cluster_compute_service->createVM(1, 0.0, "VM4");
        // myInfoSim->BareMetalList.insert(this->virtualized_cluster_compute_service->startVM(vm4, "VirtualizedClusterHost3_1"));
        // // addVmSpeed(vm4, (*myInfoSim->BareMetalList.rbegin())->getName());
        // this->core_utilization_map[*myInfoSim->BareMetalList.rbegin()] = 1;
    }





        while (true) {

         // while (not this->workflow->isDone()) {
            // If a pilot job is not running on the batch service, let's submit one that asks
            // for 3 cores on 2 compute nodes for 1 hour
            if (not pilot_job) {
                WRENCH_INFO("Creating and submitting a pilot job");
                pilot_job = job_manager->createPilotJob();
                job_manager->submitJob(pilot_job, this->batch_compute_service,
                                       {{"-N", "2"}, {"-c", "3"}, {"-t", "800000"}});
            }
            /* Get the ready tasks */
            // auto ready_tasks = this->workflow->getReadyTasks();


            // Construct the list of currently available bare-metal services (on VMs and perhaps within pilot job as well)
            std::set<std::shared_ptr<BareMetalComputeService>> available_compute_service = myInfoSim->BareMetalList;//{vm1_cs, vm2_cs, vm3_cs, vm4_cs};
            
            if (this->pilot_job_is_running) {
                /*peut etre pas a jouter dans la version final, je pense que ca peut rajouter un outils de calcule en plus de ce
                 * qu'il y a dans le xml
                 */
                // available_compute_service.insert(pilot_job->getComputeService());
            }
            
            /*HEURISTICS*/
            scheduleReadyTasks(workflow->getReadyTasks(), job_manager, available_compute_service);

            // Wait for a workflow execution event, and process it
            try {
                this->waitForAndProcessNextEvent();
            } 
            catch (ExecutionException &e) {
                WRENCH_INFO("Error while getting next execution event (%s)... ignoring and trying again",
                            (e.getCause()->toString().c_str()));
                continue;
            }
            if (this->abort || this->workflow->isDone()) {
                break;
            }
            /*ON ATTEND QUE LES TACHES SE TERMINES ET QU ELLE EXECUTE LEUR EVENEMETN DE FIN, A FAIRE AUTANT 
             *DE FOIS QU'IL Y EUT DE TACHES LANCER*/
            WRENCH_INFO("Wait for next event");
            this->waitForAndProcessNextEvent();

         }
        // S4U_Simulation::sleep(10);

        WRENCH_INFO("--------------------------------------------------------");
        if (this->workflow->isDone()) {
            WRENCH_INFO("Workflow execution is complete!");
        } else {
            WRENCH_INFO("Workflow execution is incomplete!");
        }

        WRENCH_INFO("WMS terminating");
        return 0;
    }



    /**
     * @brief Process a StandardJobFailedEvent
     *
     * @param event: a workflow execution event
     */
    void SecondeSimulation::processEventStandardJobFailure(std::shared_ptr<StandardJobFailedEvent> event) {
        auto job = event->standard_job;
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_RED);
        WRENCH_INFO("Task %s has failed", (*job->getTasks().begin())->getID().c_str());
        WRENCH_INFO("failure cause: %s", event->failure_cause->toString().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
    }



    /**
    * @brief Process a StandardJobCompletedEvent
    *
    * @param event: a workflow execution event
    */
    void SecondeSimulation::processEventStandardJobCompletion(std::shared_ptr<StandardJobCompletedEvent> event) {
        auto job = event->standard_job;
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_BLUE);
        WRENCH_INFO("Task %s has COMPLETED (on service %s)",
                    (*job->getTasks().begin())->getID().c_str(),
                    job->getParentComputeService()->getName().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
        this->core_utilization_map[job->getParentComputeService()]++;
    }

    /**
    * @brief Process a PilotJobStartedEvent event
    *
    * @param event: a workflow execution event
    */
    void SecondeSimulation::processEventPilotJobStart(std::shared_ptr<PilotJobStartedEvent> event) {
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_BLUE);
        WRENCH_INFO("The pilot job has started (it exposes bare-metal compute service %s)",
                    event->pilot_job->getComputeService()->getName().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);
        this->pilot_job_is_running = true;
        this->core_utilization_map[this->pilot_job->getComputeService()] = event->pilot_job->getComputeService()->getTotalNumIdleCores();
    }
        /**
    * @brief Process a processEventPilotJobExpiration even
    *
    * @param event: a workflow execution event
    */
    void SecondeSimulation::processEventPilotJobExpiration(std::shared_ptr<PilotJobExpiredEvent> event) {
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_RED);
        WRENCH_INFO("The pilot job has expired (it was exposing bare-metal compute service %s)",
                    event->pilot_job->getComputeService()->getName().c_str());
        TerminalOutput::setThisProcessLoggingColor(TerminalOutput::COLOR_GREEN);

        this->pilot_job_is_running = false;
        this->core_utilization_map.erase(this->pilot_job->getComputeService());
        this->pilot_job = nullptr;
    }



    /*heuristique de condor roginal, juste pour des tests*/
    void SecondeSimulation::scheduleReadyTasks(std::vector<std::shared_ptr<WorkflowTask>> ready_tasks,
                                       std::shared_ptr<JobManager> job_manager,
                                       std::set<std::shared_ptr<BareMetalComputeService>> compute_services) {

        if (ready_tasks.empty()) {
            return;
        }

        WRENCH_INFO("Trying to schedule %zu ready tasks", ready_tasks.size());



        // cerr << "corUtilMap : " << std::endl; 
        // for (auto const& cs : compute_services)
        // {
        //     cerr << cs->getName() << " " << this->core_utilization_map[cs] << " ";
        // }
        // cerr << std::endl; 
        unsigned long num_tasks_scheduled = 0;
        for (std::string taskID : savOrdo.sheduledTaskList)//pour chaque tache
        {
            for (auto const &task: ready_tasks) //on regarde si elle est dans les taches prettes
            {
                if (taskID == task->getID()) 
                {
                    for (auto const &cs: compute_services) 
                    {
                        std::string taskToDoNextForThisVm = savOrdo.getEarliestNonSheduledTaskForThisVM(cs->getName());
                        if (this->core_utilization_map[cs] > 0 && taskToDoNextForThisVm == task->getID()) 
                        {
                            // Specify that ALL files are read/written from the one storage service
                            std::map<std::shared_ptr<DataFile>, std::shared_ptr<FileLocation>> file_locations;
                            for (auto const &f: task->getInputFiles()) 
                            {
                                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
                            }
                            for (auto const &f: task->getOutputFiles()) 
                            {
                                file_locations[f] = wrench::FileLocation::LOCATION(this->storage_service);
                            }
                            try 
                            {
                                auto job = job_manager->createStandardJob(task, file_locations);
                                
                                sheduledTaskList.push_back(task->getID());
                                int tmpNbCore = 0;
                                for (auto nbFreeCore : this->core_utilization_map)
                                    tmpNbCore += nbFreeCore.second;
                                nbFreeCorePerGeneration.push_back(tmpNbCore);
                                sheduledHostList.push_back(cs->getName().c_str());
                                generations.push_back(currentGeneration);
                                
                                WRENCH_INFO(
                                        "Submitting task %s to compute service %s", task->getID().c_str(),
                                        cs->getName().c_str());
                                job_manager->submitJob(job, cs);
                                this->core_utilization_map[cs]--;
                                num_tasks_scheduled++;
                                // scheduled = true;
                                savOrdo.setTaskAsShceduled(task->getID());/*marquer la tache comme scheduled si ca marche*/
                            } catch (ExecutionException &e) 
                            {
                                WRENCH_INFO("WARNING: Was not able to submit task %s, likely due to the pilot job having expired "
                                            "(I should get a notification of its expiration soon)",
                                            task->getID().c_str());
                            }
                            break;
                        }
                    }
                }
            }
        }
        currentGeneration++;
        WRENCH_INFO("Was able to schedule %lu out of %zu ready tasks", num_tasks_scheduled, ready_tasks.size());

            // if (this->core_utilization_map[cs] == numberOfCoresBeforeCheck)
            //     break;//if there was no task fit for this vm, go to the next one
            // }
    }
    

}// namespace wrench
