#include "SavedOrdonancement.h"



SavedOrdonancement::SavedOrdonancement() {

}


/**
 * @brief      Constructs a new instance.
 *
 * @param[in]  sheduledTaskList  The sheduled task list
 * @param[in]  sheduledHostList  The sheduled host list
 * @param[in]  generations       The generations
 */
SavedOrdonancement::SavedOrdonancement(std::vector<std::string> sheduledTaskList,
					    std::vector<std::string> sheduledHostList,
						std::vector<int> generations,
                        std::vector<int> nbFreeCorePerGeneration) {

    std::vector<std::string>::iterator itT = sheduledTaskList.begin();
    std::vector<std::string>::iterator itH = sheduledHostList.begin();
    std::vector<int>::iterator itG = generations.begin();
    std::vector<int>::iterator itC = nbFreeCorePerGeneration.begin();
    for (; itT != sheduledTaskList.end() && itH != sheduledHostList.end() && itG != generations.end() && itC != nbFreeCorePerGeneration.end(); itH++, itT++, itG++, itC++)
    {
    	this->sheduledTaskList.push_back(*itT);
    	this->sheduledHostList.push_back(*itH);
    	this->generations.push_back(*itG);
    	this->nbFreeCorePerGeneration.push_back(*itC);;
        this->sheduled.push_back(false);
    }


}



/**
 * @brief      Serialize vectors into a string
 *
 * @return     the serialized vector
 */
std::string SavedOrdonancement::serialize() {
    std::ostringstream oss;
    std::vector<std::string>::iterator itT = sheduledTaskList.begin();
    std::vector<std::string>::iterator itH = sheduledHostList.begin();
    std::vector<int>::iterator itG = generations.begin();
    std::vector<bool>::iterator itS = sheduled.begin();
    while (itT != sheduledTaskList.end() && itH != sheduledHostList.end() && itG != generations.end() && itS != sheduled.end()) {
        oss << *itT << " ";itT++;
        oss << *itH << " ";itH++;
        oss << *itG << " ";itG++;
        oss << *itS << " ";itS++;
    }
    return oss.str();
}


/**
 * @brief      Deserialize a string into vectors
 *
 * @param      str   The string
 */
void SavedOrdonancement::deserialize(std::string& str) {
    std::istringstream iss(str);
    string sheduledTaskListEllem;
    string sheduledHostListEllem;
    int generationsEllem;
    bool sheduledEllem;
    while (iss >> sheduledTaskListEllem >> sheduledHostListEllem >> generationsEllem >> sheduledEllem ) {
        sheduledTaskList.push_back(sheduledTaskListEllem);
        sheduledHostList.push_back(sheduledHostListEllem);
        generations.push_back(generationsEllem);
        sheduled.push_back(sheduledEllem);
    }
}


/**
 * @brief      prints the task in the order they where sheduluded and grouped by generation
 * a generation being all the tasks that where sheduled in the same call of scheduleReadyTasks
 * 
 * only used for debugging
 */
void SavedOrdonancement::print() {
	std::cerr << std::endl << "ids : " << std::endl;
	std::vector<std::string>::iterator itT = sheduledTaskList.begin();
	std::vector<std::string>::iterator itH = sheduledHostList.begin();
	std::vector<int>::iterator itG = generations.begin();
    std::vector<int>::iterator itC = nbFreeCorePerGeneration.begin();
	int gen = -1;
	for (; itT != sheduledTaskList.end() && itH != sheduledHostList.end() && itG != generations.end(); itH++, itT++, itG++)
	{
	    if (gen < *itG)
	    {
	        std::cerr << "----------------------------------------" << std::endl << std::endl;
	        std::cerr << "generation : " << *itG << std::endl;
            if (itC < nbFreeCorePerGeneration.end()) {
                std::cerr << "number of free cores " << *itC << std::endl << std::endl;
            }
	        gen = *itG;
	    }
	    std::cerr << *itT << std::endl;
	    std::cerr << *itH << std::endl << std::endl;

	}
}

/**
 * @brief      saves the ordonancement in a file in the same format it is print as, mostly used in conjonction with the 
 * diff command to make sur that two ordonanacement were executed in the exact same way
 *
 * @param[in]  path  The path the file will be saved at
 */
void SavedOrdonancement::printToFile(const string& path) {
    std::ofstream outputFile(path);  // Open the file for writing

    if (outputFile.is_open()) {
        outputFile << std::endl << "ids : " << std::endl;
        std::vector<std::string>::iterator itT = sheduledTaskList.begin();
        std::vector<std::string>::iterator itH = sheduledHostList.begin();
        std::vector<int>::iterator itG = generations.begin();
        std::vector<int>::iterator itC = nbFreeCorePerGeneration.begin();
        int gen = -1;
        for (; itT != sheduledTaskList.end() && itH != sheduledHostList.end() && itG != generations.end(); itH++, itT++, itG++, itC++) {
            if (gen < *itG) {
                outputFile << "----------------------------------------" << std::endl << std::endl;
                outputFile << "generation : " << *itG << std::endl;
                if (itC < nbFreeCorePerGeneration.end()) {
                    outputFile << "number of free cores " << *itC << std::endl << std::endl;
                }
                gen = *itG;
            }
            outputFile << *itT << std::endl;
            outputFile << *itH << std::endl << std::endl;
        }

        outputFile.close();  // Close the file after writing
        std::cout << "Output saved to file " << path << std::endl;
    } else {
        std::cerr << "Unable to open the file for writing" << std::endl;
    }
}



/**
 * @brief      Finds a task id.
 *
 * @param[in]  HostName  The host name
 *
 * @return     The host name
 */
std::string SavedOrdonancement::findTaskID(std::string HostName) {
    std::vector<std::string>::iterator itT = sheduledTaskList.begin();
    std::vector<std::string>::iterator itH = sheduledHostList.begin();
    while (itT != sheduledTaskList.end() && itH != sheduledHostList.end() && *itH != HostName) {
        itH++;
        itT++;
    }
    if (itH != sheduledHostList.end()) {
        return *itT;
    } else {
        return "hostname not found";
    }
}



/**
 * @brief      Finds a host name.
 *
 * @param[in]  TaskID  The task id
 *
 * @return     The task id
 */
std::string SavedOrdonancement::findHostName(std::string TaskID) {
	std::vector<std::string>::iterator itT = sheduledTaskList.begin();
	std::vector<std::string>::iterator itH = sheduledHostList.begin();
	while (itT != sheduledTaskList.end() && itH != sheduledHostList.end() && *itT != TaskID)
	{
		itH++;
		itT++;
	}
	if (itH != sheduledHostList.end())
		return *itH;
	else
		return "TaskID not found";
}



/**
 * @brief      Gets the earliest non sheduled task for this vm.
 *
 * @param[in]  HostName  The host name
 *
 * @return     The earliest non sheduled task for this vm.
 */
std::string SavedOrdonancement::getEarliestNonSheduledTaskForThisVM(std::string HostName) {
	std::vector<std::string>::iterator itT = sheduledTaskList.begin();
	std::vector<std::string>::iterator itH = sheduledHostList.begin();
    std::vector<bool>::iterator itS = sheduled.begin(); 

	// while (itT != sheduledTaskList.end() &&
	// 	   itH != sheduledHostList.end() &&
	// 	   itS != sheduled.end() )
	for (size_t i = 0; i < sheduledHostList.size(); ++i)
	{

		if ((*itH == HostName && !*itS)/*already sheduled tasks are ignored */)
			return *itT;
		itH++;
		itT++;
		itS++;
	}
	if (itT != sheduledTaskList.end())
		return *itT;
	else
		return "TaskID not found";
}

/**
 * @brief      Sets the task as shceduled.
 *
 * @param[in]  TaskID  The task id
 */
void SavedOrdonancement::setTaskAsShceduled(std::string TaskID) {
	std::vector<std::string>::iterator itT = sheduledTaskList.begin();
    std::vector<bool>::iterator itS = sheduled.begin(); 
	while (itT != sheduledTaskList.end() &&
		   itS != sheduled.end() && 
		   *itT != TaskID)
	{
		itT++;
		itS++;
	}
	if (itT != sheduledTaskList.end())
		*itS = true;
	else
		std::cerr << "TaskID not found" << std::endl;
}


