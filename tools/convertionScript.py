import xml.etree.ElementTree as ET
import json
import sys


if len(sys.argv) != 2 and len(sys.argv) != 3 :
    print("Usage: python script.py input_file.xml [output_file.json]")
    sys.exit(1)


#TODO CHECK SI LES FICHIER SONT DU BON TYPE


xmlName = sys.argv[1]
outputFile = "convertedJson.json"
if len(sys.argv) == 3 :
    outputFile = sys.argv[2]

##ajouter des test pour verifier que le fichier xml existe et est du bon type
##voir si je peut recuperer les info en commentaire
# xmlName = 'oldXml.xml'

tree = ET.parse(xmlName)
xmlroot = tree.getroot()

jsonObject= {}
jsonObject['name'] = xmlroot.attrib['name']
jsonObject['description'] = 'instance generated from the file'
jsonObject['createdAt'] = 'NOT IMPLEMENTED IN THE CONVERTION SCRYPT'
jsonObject['schemaVersion'] =  xmlroot.attrib['version']


jsonObject['author'] = {
    'name': 'NOT IMPLEMENTED IN THE CONVERTION SCRYPT',
    'email': 'NOT IMPLEMENTED IN THE CONVERTION SCRYPT'
}

jsonObject['wms'] = {
    'name': 'NOT IMPLEMENTED IN THE CONVERTION SCRYPT',
    'version': 'NOT IMPLEMENTED IN THE CONVERTION SCRYPT',
    'url':'NOT IMPLEMENTED IN THE CONVERTION SCRYPT'
}


nbTasks = int(xmlroot.attrib['jobCount'])
jsonObject['workflow'] = {
    'executedAt' : 'NOT IMPLEMENTED IN THE CONVERTION SCRYPT',
    'makespan' : 0,
    'tasks': [{} for _ in range(nbTasks)]
    ##'tasks' : {[{} for x in range(nbTasks)]}
}
##jsonObject['workflow']['task'] = {[{} for x in range(nbTasks)]}

indexNbParent = 0
parentCount = {}  # Number of parents for each child ID

for t in xmlroot:
    if 'ref' in t.attrib:
        for ch in t:
            ref = ch.attrib.get('ref')[2:]
            parentCount[ref] = parentCount.get(ref, 0) + 1
            
  

# Creation of a correspondence table
tabCorrespondence = [{'name': '', 'id': '', 'fullName': ''} for _ in range(len(xmlroot))] 
indexCorrespondence = 0
for t in xmlroot:
    if 'name' in t.attrib:
        tabCorrespondence[indexCorrespondence].update({
            'name': t.attrib.get('name', ''),
            'id': t.attrib.get('id', '')[2:],
            'fullName': t.attrib.get('name', '') + t.attrib.get('id', '')[2:],
            'index' : indexCorrespondence
        })
        indexCorrespondence += 1   
        
        
def find_entry_fullname(tabCorrespondence, id):
    for entry in tabCorrespondence:
        if entry['id'] == id:
            return entry['fullName']
    return None
        
def find_entry_index(tabCorrespondence, fullName):
    for entry in tabCorrespondence:
        if entry['fullName'] == fullName:
            return entry['index']
    return None

index = 0
for taskOrParentage in xmlroot:
    if 'name' in taskOrParentage.attrib:
        taskId = taskOrParentage.attrib['id'][2:]
        taskName = taskOrParentage.attrib['name']
        jsonObject['workflow']['tasks'][index] = {
            'name': taskName + taskId,
            'type': 'compute',
            'runtime': float(taskOrParentage.attrib['runtime']),
            'command': {'program': taskName},
            'parents': [],
            'children': [],
            'files': [{} for _ in range(len(taskOrParentage))],
            'cores': 1,
            'id': taskId,
            'category': taskName
        }
        indexFile = 0
        for file in taskOrParentage:
            jsonObject['workflow']['tasks'][index]['files'][indexFile] = {
                'link': file.attrib['link'],
                'name': file.attrib['file'],
                'size': int(file.attrib['size'])
            }
            indexFile += 1
        index += 1
    else:
        if not 'ref' in taskOrParentage.attrib:
            print("THERE IS A TYPE OF DATA THAT I FORGOT TO TREAT")
            print(taskOrParentage.attrib)

for taskOrParentage in xmlroot :
    if 'ref' in taskOrParentage.attrib:
        childRef = taskOrParentage.attrib['ref'][2:]
        parents = [find_entry_fullname(tabCorrespondence,ch.attrib['ref'][2:]) for ch in taskOrParentage]
        for ell in jsonObject['workflow']['tasks']:
            if ell['id'] == childRef:
                ell['parents'] = parents
        for par in parents:
            for ell in jsonObject['workflow']['tasks']:
                if par == ell['name']:
                    ell['children'].append(find_entry_fullname(tabCorrespondence,childRef))
                    
                    
def print_correspondence_table(tabCorrespondence):
    for corr in tabCorrespondence:
        print('name : ' + corr['name'])
        print('id : ' + corr['id'])
        print('fullName : ' + corr['fullName'])
        print()
        
def print_children_lists(json_object):
    for task in json_object['workflow']['tasks']:
        task_id = task['id']
        task_name = task['name']
        children = task['children']
        print(f"Children of task '{task_name}' (ID: {task_id}):")
        if children:
            for child in children:
                print(f" - {child}")
        else:
            print(" - No children")
        parents = task['parents']
        print(f"parents of task '{task_name}' (ID: {task_id}):")
        if parents:
            for parent in parents:
                print(f" - {parent}")
        else:
            print(" - No parents")
        print()
        print()
        print()

# Example usage
# print_children_lists(jsonObject)

json_data = json.dumps(jsonObject)

# outputFile = "/home/nanachibestwaifu/WORKDIR/wrench-2.1/build/examples/workflow_api/real-workflow-example/convertedJson.json"


with open(outputFile, "w") as file:
    file.write(json_data)

file.close()
#json_data



