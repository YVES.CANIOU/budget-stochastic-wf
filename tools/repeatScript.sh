#!/bin/bash

# Function to replace occurrences of "NUMBER1" and "NUMBER2" with incrementing numbers
replace_numbers() {
  local input_text="$1"
  local count1="$2"
  local count2="$3"
  local output_file="$4"

  for ((i=1; i<=count1; i++)); do
    for ((j=1; j<=count2; j++)); do
      echo "${input_text//NUMBER1/$i}" | sed "s/NUMBER2/$j/g" >> "$output_file"
    done
  done
}

# Usage: ./script.sh "text to replace NUMBER1 and NUMBER2" 3 2 output.txt
if [ $# -ne 4 ]; then
  echo "Usage: $0 <input_text> <count1> <count2> <output_file>"
  exit 1
fi

input_text="$1"
count1="$2"
count2="$3"
output_file="$4"

replace_numbers "$input_text" "$count1" "$count2" "$output_file"

