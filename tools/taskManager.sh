#!/bin/bash

FUTURE_DIR="futur"
PRESENT_DIR="present"
PAST_DIR="past"
MAX_TASKS=5

failed_files=() # List to track files for which command execution failed

while true; do
  # Count the number of running tasks
  running_tasks=$(pgrep -c -f "wrench-example-condor-grid-universe")

  if [ $running_tasks -lt $MAX_TASKS ]; then
    # Check if there are any files in the future directory
    if [ -n "$(find "$FUTURE_DIR" -maxdepth 1 -type f)" ]; then
      # Move a file from future to present directory
      future_file=$(find "$FUTURE_DIR" -maxdepth 1 -type f -print -quit)
      mv "$future_file" "$PRESENT_DIR"

      # Check if the file is in the failed list
      if [[ " ${failed_files[@]} " =~ " ${future_file} " ]]; then
        # Skip the file if it failed previously
        continue
      fi

      # Execute the file in a new shell
      (
        ./wrench-example-condor-grid-universe "$PRESENT_DIR/$(basename "$future_file")"

        # Check the exit status of the command
        if [ $? -eq 0 ]; then
          # Command execution successful, move the file to the past directory
          mv "$PRESENT_DIR/$(basename "$future_file")" "$PAST_DIR"
        else
          # Command execution failed, move the file back to the future directory
          # mv "$PRESENT_DIR/$(basename "$future_file")" "$FUTURE_DIR"
          failed_files+=("$future_file") # Add the file to the failed list
        fi
      ) &  # Run the command in the background
    fi
  fi

# Check if the future directory is empty and the present directory only contains files in the failed_files list
if [ -z "$(find "$FUTURE_DIR" -maxdepth 1 -type f)" ]; then
  count=0

  # Iterate over files in the directory
  for file in "$PRESENT_DIR"/*; do
    # Check if the file is not in the given list
    if [[ ! " ${failed_files[@]} " =~ " ${file} " ]]; then
      count=$((count + 1))
    fi
  done
  
  if [ $count -eq 0 ]; then
    break
  fi
fi





  sleep 1  # Adjust the sleep duration as needed
done

