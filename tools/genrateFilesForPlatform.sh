#!/bin/bash

# Check the number of arguments
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <number_of_hosts_types> <number_of_hosts_per_type>"
    exit 1
fi


rm pltf.xml
    

number_of_hosts_type=$1
number_of_hosts_per_type=$2


    
system_hosts="<?xml version='1.0'?>
<!DOCTYPE platform SYSTEM \"https://simgrid.org/simgrid.dtd\">
<platform version=\"4.1\">
    <zone id=\"AS0\" routing=\"Full\">

        <!-- The host on which the WMS will run -->
        <host id=\"WMSHost\" speed=\"1Mf\" core=\"1\">
            <disk id=\"hard_drive\" read_bw=\"100MBps\" write_bw=\"100MBps\">
                <prop id=\"size\" value=\"5000GiB\"/>
                <prop id=\"mount\" value=\"/\"/>
            </disk>
        </host>

        <host id=\"BatchHeadNode\" speed=\"10Mf\" core=\"1\"/>
        <host id=\"BatchNode1\" speed=\"50Mf\" core=\"10\"/>
        <host id=\"BatchNode2\" speed=\"50Mf\" core=\"10\"/>

        <!-- The host on which the VirtualizedClusterComputeService will run -->
        <host id=\"VirtualizedClusterProviderHost\" speed=\"100Mf\" core=\"100000\">    
            <disk id=\"hard_drive\" read_bw=\"10000MBps\" write_bw=\"100MBps\">
                <prop id=\"size\" value=\"5000GiB\"/>
                <prop id=\"mount\" value=\"/SCRATCH/\"/>
            </disk>
       </host>





"
system_links="
    







<!-- A network link -->
<link id=\"network_link\" bandwidth=\"50MBps\" latency=\"20us\"/>
<!-- WMSHost's local \"loopback\" link -->
<link id=\"loopback_WMSHost\" bandwidth=\"1000EBps\" latency=\"0us\"/>

<!--VirtualizedClusterProviderHost's local \"loopback\" link -->
<link id=\"loopback_VirtualizedClusterProviderHost\" bandwidth=\"1000EBps\" latency=\"0us\"/>
<!--VirtualizedClusterHost1's local \"loopback\" link -->
<!-- <link id=\"loopback_VirtualizedClusterHost1\" bandwidth=\"1000EBps\" latency=\"0us\"/> -->
<!--VirtualizedClusterHost2's local \"loopback\" link -->
<!-- <link id=\"loopback_VirtualizedClusterHost2\" bandwidth=\"1000EBps\" latency=\"0us\"/> -->

<!--VirtualizedClusterProviderHost's local \"loopback\" link -->
<link id=\"BatchHeadNode\" bandwidth=\"1000EBps\" latency=\"0us\"/>
<!--VirtualizedClusterHost1's local \"loopback\" link -->
<link id=\"BatchNode1\" bandwidth=\"1000EBps\" latency=\"0us\"/>
<!--VirtualizedClusterHost2's local \"loopback\" link -->
<link id=\"BatchNode2\" bandwidth=\"1000EBps\" latency=\"0us\"/>







"
system_routes="











        <!-- The network link connects the two hosts -->
        <route src=\"WMSHost\" dst=\"VirtualizedClusterProviderHost\"> <link_ctn id=\"network_link\"/> </route>
<!--         <route src=\"WMSHost\" dst=\"VirtualizedClusterHost1\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"WMSHost\" dst=\"VirtualizedClusterHost2\"> <link_ctn id=\"network_link\"/> </route> -->

        <!-- The network link connects the two hosts -->
        <route src=\"WMSHost\" dst=\"BatchHeadNode\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"WMSHost\" dst=\"BatchNode1\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"WMSHost\" dst=\"BatchNode2\"> <link_ctn id=\"network_link\"/> </route>

        <route src=\"BatchHeadNode\" dst=\"BatchNode1\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"BatchHeadNode\" dst=\"BatchNode2\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"BatchNode1\" dst=\"BatchNode2\"> <link_ctn id=\"network_link\"/> </route>
<!-- 
        <route src=\"VirtualizedClusterProviderHost\" dst=\"VirtualizedClusterHost1\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"VirtualizedClusterProviderHost\" dst=\"VirtualizedClusterHost2\"> <link_ctn id=\"network_link\"/> </route>
        <route src=\"VirtualizedClusterHost1\" dst=\"VirtualizedClusterHost2\"> <link_ctn id=\"network_link\"/> </route>
 -->
        <!-- Each loopback link connects each host to itself -->
        <route src=\"WMSHost\" dst=\"WMSHost\">
            <link_ctn id=\"loopback_WMSHost\"/>
        </route>
        
        <route src=\"VirtualizedClusterProviderHost\" dst=\"VirtualizedClusterProviderHost\">
            <link_ctn id=\"loopback_VirtualizedClusterProviderHost\"/>
        </route>
<!--         <route src=\"VirtualizedClusterHost1\" dst=\"VirtualizedClusterHost1\">
            <link_ctn id=\"loopback_VirtualizedClusterHost1\"/>
        </route>
        <route src=\"VirtualizedClusterHost2\" dst=\"VirtualizedClusterHost2\">
            <link_ctn id=\"loopback_VirtualizedClusterHost2\"/>
        </route> -->

        <route src=\"BatchHeadNode\" dst=\"BatchHeadNode\">
            <link_ctn id=\"network_link\"/>
        </route>
        <route src=\"BatchNode1\" dst=\"BatchNode1\">
            <link_ctn id=\"network_link\"/>
        </route>
        <route src=\"BatchNode2\" dst=\"BatchNode2\">
            <link_ctn id=\"network_link\"/>
        </route>






"



system_endfile="









<!--
    rm outputs/outputHost.txt outputs/outputLinks.txt outputs/outputRoute.txt



    ./repeatScript.sh '<host id=\"VirtualizedClusterHostNUMBER1_NUMBER2\" speed=\"100Mf\" core=\"1\">
    <prop id=\"ram\" value=\"64GB\" />
</host>

    ' 3 3 outputs/outputHost.txt



    ./repeatScript.sh '<link id=\"loopback_VirtualizedClusterHostNUMBER1_NUMBER2\" bandwidth=\"1000EBps\" latency=\"0us\"/>
<link id=\"link_VirtualizedClusterHostNUMBER1_NUMBER2_VirtualizedClusterProviderHost\" bandwidth=\"50MBps\" latency=\"20us\"/>
<link id=\"link_VirtualizedClusterHostNUMBER1_NUMBER2_WMSHost\" bandwidth=\"50MBps\" latency=\"20us\"/>




        ' 3 3 outputs/outputLinks.txt




    ./repeatScript.sh '        <route src=\"VirtualizedClusterHostNUMBER1_NUMBER2\" dst=\"VirtualizedClusterHostNUMBER1_NUMBER2\">
            <link_ctn id=\"loopback_VirtualizedClusterHostNUMBER1_NUMBER2\"/>
        </route>

        <route src=\"VirtualizedClusterHostNUMBER1_NUMBER2\" dst=\"VirtualizedClusterProviderHost\">
            <link_ctn id=\"link_VirtualizedClusterHostNUMBER1_NUMBER2_VirtualizedClusterProviderHost\"/>
        </route>

        <route src=\"WMSHost\" dst=\"VirtualizedClusterHostNUMBER1_NUMBER2\"> <link_ctn id=\"link_VirtualizedClusterHostNUMBER1_NUMBER2_WMSHost\"/> </route>' 3 3 outputs/outputRoute.txt
 -->



    </zone>
</platform>
"


echo "$system_hosts" >> pltf.xml


    ./repeatScript.sh '<host id="VirtualizedClusterHostNUMBER1_NUMBER2" speed="100Mf" core="1">
    <prop id="ram" value="64GB" />
</host>

    ' $number_of_hosts_type $number_of_hosts_per_type pltf.xml
    
   
echo "$system_links" >> pltf.xml



    ./repeatScript.sh '<link id="loopback_VirtualizedClusterHostNUMBER1_NUMBER2" bandwidth="1000EBps" latency="0us"/>
<link id="link_VirtualizedClusterHostNUMBER1_NUMBER2_VirtualizedClusterProviderHost" bandwidth="50MBps" latency="20us"/>
<link id="link_VirtualizedClusterHostNUMBER1_NUMBER2_WMSHost" bandwidth="50MBps" latency="20us"/>




        ' $number_of_hosts_type $number_of_hosts_per_type pltf.xml

echo "$system_routes" >> pltf.xml



    ./repeatScript.sh '        <route src="VirtualizedClusterHostNUMBER1_NUMBER2" dst="VirtualizedClusterHostNUMBER1_NUMBER2">
            <link_ctn id="loopback_VirtualizedClusterHostNUMBER1_NUMBER2"/>
        </route>

        <route src="VirtualizedClusterHostNUMBER1_NUMBER2" dst="VirtualizedClusterProviderHost">
            <link_ctn id="link_VirtualizedClusterHostNUMBER1_NUMBER2_VirtualizedClusterProviderHost"/>
        </route>

        <route src="WMSHost" dst="VirtualizedClusterHostNUMBER1_NUMBER2"> <link_ctn id="link_VirtualizedClusterHostNUMBER1_NUMBER2_WMSHost"/> </route>' $number_of_hosts_type $number_of_hosts_per_type pltf.xml



echo "$system_endfile" >> pltf.xml